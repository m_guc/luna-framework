package org.lunaframework.data.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;

public class LimitAwareResultSetExtractor<T> {

	private RowMapper<T> rowMapper;

	public LimitAwareResultSetExtractor(RowMapper<T> rowMapper) {
		this.rowMapper = rowMapper;
	}

	public List<T> extractData(ResultSet result, int limit) {
		ArrayList<T> list = new ArrayList<T>();
		try {
			int rowNum = 0;
			while (result.next()) {
				if (rowNum >= limit) {
					break;
				}
				list.add(rowMapper.mapRow(result, rowNum));
				rowNum++;
			}
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		return (List<T>) list;
	}
}
