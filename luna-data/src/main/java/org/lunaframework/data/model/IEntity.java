package org.lunaframework.data.model;

import java.io.Serializable;

public interface IEntity {

	public Serializable getId();

}
