package org.lunaframework.data.model;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.envers.Audited;
import org.lunaframework.core.model.AbstractEntity;
import org.lunaframework.data.audit.Audit;

@MappedSuperclass
public class BaseEntity extends AbstractEntity implements IEntity {

	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	@Column(name = "ID")
	private String id;

	@Version
	private int version;

	@Audited
	@Embedded
	private Audit auditLog = new Audit();

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public Audit getAuditLog() {
		return auditLog;
	}

	public void setAuditLog(Audit auditLog) {
		this.auditLog = auditLog;
	}

}
