package org.lunaframework.data.audit;

import java.util.Date;

import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.lunaframework.core.model.AbstractEntity;
import org.lunaframework.core.model.Identity;
import org.lunaframework.core.model.ToString;

@Embeddable
@Identity(values = { "createdBy", "creationTime" })
@ToString(values = { "createdBy", "creationTime", "updatedBy", "updateTime" })
public class Audit extends AbstractEntity {

	private String createdBy;
	@Temporal(TemporalType.TIMESTAMP)
	private Date creationTime;

	private String updatedBy;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateTime;

	public Audit() {
	}

	public Audit(String createdBy, Date creationTime) {
		super();
		this.createdBy = createdBy;
		this.creationTime = creationTime;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(Date creationTime) {
		this.creationTime = creationTime;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

}
