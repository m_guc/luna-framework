package org.lunaframework.data.dao;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.hibernate.CacheMode;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.LockOptions;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.lunaframework.data.model.IEntity;

public class EntityDaoImpl implements EntityDao {

	private SessionFactory sessionFactory;

	@Override
	public <T> T getEntity(Class<T> entityClass, Serializable id) {
		T t = (T) getSessionFactory().getCurrentSession().get(entityClass, id);
		return t;
	}

	@Override
	public <T> T loadEntity(Class<T> entityClass, Serializable id) {
		return (T) getSessionFactory().getCurrentSession().load(entityClass, id,LockOptions.READ);
	}

	@Override
	public <T> List<T> getEntities(Class<T> entityClass) {
		Criteria createCriteria = getSessionFactory().getCurrentSession().createCriteria(entityClass);
		return createCriteria.list();
	}

	@Override
	public <T> T createEntity(T entity) {
		getSessionFactory().getCurrentSession().saveOrUpdate(entity);
		return entity;
	}

	@Override
	public <T> void deleteEntity(T entity) {
		getSessionFactory().getCurrentSession().delete(entity);
	}

	@Override
	public <T> T mergeEntity(T entity) {
		return (T) getSessionFactory().getCurrentSession().merge(entity);
	}

	@Override
	public <T> T updateEntity(T entity) {
		getSessionFactory().getCurrentSession().update(entity);
		return entity;
	}

	@Override
	public <T> void refreshEntity(T entity) {
		getSessionFactory().getCurrentSession().refresh(entity);
	}

	@Override
	public Query createQuery(String queryName) {
		return getSessionFactory().getCurrentSession().createQuery(queryName);
	}

	@Override
	public <T extends Object> List<T> findByQuery(String queryName, Object... params) {
		return findByQuery(getSessionFactory().getCurrentSession().createQuery(queryName), params);
	}

	private <T extends Object> List<T> findByQuery(Query query, Object... params) {
		if (query == null)
			return null;

		if (params != null) {
			for (int i = 0; i < params.length; i++) {
				query.setParameter(i, params[i]);
			}
		}
		return query.list();
	}

	private <T> List<T> findByNamedParams(String queryName, String[] paramNames, Object[] paramValues, Integer start,
			Integer limit) {
		Query query = getSessionFactory().getCurrentSession().createQuery(queryName);
		query.setCacheable(false);

		if (start != null) {
			query.setFirstResult(start);
		}

		if (limit != null) {
			query.setMaxResults(limit);
		}
		return findByNamedParams(query, paramNames, paramValues);
	}

	private <T> List<T> findByNamedParams(Query query, String[] paramNames, Object[] paramValues) {
		applyParameters(query, paramNames, paramValues);
		return query.list();
	}

	private void applyParameters(Query query, String[] paramNames, Object[] paramValues) {
		if (query == null || paramNames == null || paramValues == null) {
			return;
		}
		for (int i = 0; i < paramNames.length; i++) {
			String paramName = paramNames[i];
			Object paramValue = paramValues[i];
			applyNamedParameterToQuery(query, paramName, paramValue);
		}
	}

	protected void applyNamedParameterToQuery(Query queryObject, String paramName, Object value)
			throws HibernateException {

		if (value instanceof Collection) {
			queryObject.setParameterList(paramName, (Collection) value);
		} else if (value instanceof Object[]) {
			queryObject.setParameterList(paramName, (Object[]) value);
		} else {
			queryObject.setParameter(paramName, value);
		}
	}

	@Override
	public <T> List<T> findByNamedQuery(String queryName) {
		Query query = getSessionFactory().getCurrentSession().getNamedQuery(queryName);
		query.setCacheable(true);
		if (query == null)
			return null;
		return query.list();
	}

	@Override
	public <T> List<T> findByNamedQuery(String queryName, Object... params) {
		return findByQuery(getSessionFactory().getCurrentSession().getNamedQuery(queryName), params);
	}

	@Override
	public <T> List<T> findByQueryIn(Class<T> entityClass, String queryName, List<String> ids) {
		Query query = getSessionFactory().getCurrentSession().createQuery(queryName);
		query.setCacheable(true);
		query.setParameterList("ids", ids);
		return query.list();
	}

	@Override
	public <T> T reloadByID(T entity) {
		return (T) getEntity(entity.getClass(), ((IEntity) entity).getId());
	}

	private Query createQueryWithGivenParams(String queryName, Object... params) {
		Query query = getSessionFactory().getCurrentSession().createQuery(queryName);
		query.setCacheable(true).setCacheMode(CacheMode.NORMAL);
		if (params != null) {
			for (int i = 0; i < params.length; i++) {
				query.setParameter(i, params[i]);
			}
		}
		return query;
	}

	@Override
	public <T> T getUniqueResult(String queryName, Object... params) {
		Query query = createQueryWithGivenParams(queryName, params);
		query.setCacheable(false);

		return (T) query.uniqueResult();
	}

	@Override
	public int getCount(String hqlQuery, Map<String, Object> params) {
		return getCount(hqlQuery, params.keySet().toArray(new String[0]), params.values().toArray(new Object[0]));
	}

	@Override
	public int getCount(String hqlQuery, String[] paramNames, Object[] paramValues) {
		Query query = createQuery(hqlQuery);
		applyParameters(query, paramNames, paramValues);
		Number result = (Number) query.uniqueResult();

		if (!(result instanceof Number)) {
			return -1;
		}

		return result.intValue();
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public <T> List<T> findByNamedParams(String query, Map<String, Object> params) {
		return findByNamedParams(query, params.keySet().toArray(new String[0]), params.values().toArray(new Object[0]),
				null, null);
	}

	@Override
	public <T> List<T> findByNamedParams(String query, Map<String, Object> params, Integer start, Integer limit) {
		return findByNamedParams(query, params.keySet().toArray(new String[0]), params.values().toArray(new Object[0]),
				start, limit);
	}

}
