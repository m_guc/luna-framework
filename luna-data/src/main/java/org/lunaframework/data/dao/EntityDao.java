package org.lunaframework.data.dao;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;

public interface EntityDao {

	public <T> T getEntity(Class<T> classz, Serializable id);

	public <T> T loadEntity(Class<T> entityClass, Serializable id);

	public <T> List<T> getEntities(Class<T> classz);

	public <T> T createEntity(T entity);

	public <T> void deleteEntity(T entity);

	public <T> T mergeEntity(T entity);

	public <T> T updateEntity(T entity);

	public <T> void refreshEntity(T entity);

	public <T extends Object> List<T> findByQuery(String queryName, Object... params);

	public <T> List<T> findByNamedQuery(String queryName);

	public <T> List<T> findByNamedQuery(String queryName, Object... params);

	public <T> List<T> findByQueryIn(Class<T> entityClass, String queryName, List<String> ids);

	public <T> List<T> findByNamedParams(String queryName, Map<String, Object> params);

	public <T> T reloadByID(T entity);

	public <T> T getUniqueResult(String queryName, Object... params);

	public Query createQuery(String queryName);

	public <T> List<T> findByNamedParams(String query, Map<String, Object> params, Integer start, Integer limit);

	int getCount(String hqlQuery, Map<String, Object> params);

	int getCount(String hqlQuery, String[] paramNames, Object[] paramValues);

}
