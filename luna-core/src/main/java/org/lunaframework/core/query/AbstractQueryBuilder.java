package org.lunaframework.core.query;

import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import org.apache.commons.lang.StringUtils;
import org.lunaframework.core.model.AbstractEntity;
import org.lunaframework.core.model.ToString;
import org.lunaframework.core.util.UtilsForReflection;

@ToString(values = { "params", "conditions" })
public abstract class AbstractQueryBuilder<T extends Kriter> extends AbstractEntity {

	private static final Logger logger = Logger.getLogger(AbstractQueryBuilder.class.getCanonicalName());

	private LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
	private LinkedList<String> conditions = new LinkedList<String>();
	private LinkedHashSet<String> joins = new LinkedHashSet<String>();
	private T kriter;

	public abstract boolean canBuild(Kriter kriter);

	public void build(T kriter) {
		this.kriter = kriter;
		params = new LinkedHashMap<String, Object>();
		conditions = new LinkedList<String>();
		joins = new LinkedHashSet<String>();

		addQueryConditions();

		logger.info("params : " + getParams());
		logger.info("query string : " + buildQuery());
	}

	/**
	 * Hibernate session factory ile kullanılmalı, JDBC ile kullanıldıgında
	 * method override edilip JDBC için sorgu ifadesi oluşturulmalı
	 * 
	 * @return
	 */
	protected String buildQuery() {
		return getQuerySelectPart() + getQueryJoinPart() + getQueryConditionPart();
	}

	protected String getQuerySelectPart() {
		return "select distinct e from " + getEntityClassName() + " e ";
	}

	protected String getQueryJoinPart() {
		if (getJoins() == null || getJoins().size() == 0) {
			return "";
		}
		return StringUtils.join(getJoins(), " ");
	}

	protected String getQueryConditionPart() {
		if (getConditions() == null || getConditions().size() == 0) {
			return "";
		}

		return " where 1=1 " + StringUtils.join(getConditions(), " ");
	}

	/**
	 * Hibernate Entityleri için bir önemi var, Jdbc kullanıldıgı zaman buildHq
	 * methodu override edilmeli
	 * 
	 * @return
	 */
	protected abstract String getEntityClassName();

	protected boolean isNotEmpty(String param) {
		return param != null && param.trim().length() > 0;
	}

	protected abstract void addQueryConditions();

	protected void addCondition(String fieldName, String conditionString, Object value) {
		fieldName = fieldName.replace(".", "");
		conditions.add(StringUtils.replace(conditionString, "*", ":" + fieldName));
		params.put(fieldName, value);
	}

	protected void addCondition(String conditionString) {
		conditions.add(conditionString);
	}

	protected void addCondition(String fieldName, String conditionString) {
		Object value = UtilsForReflection.getValue(kriter, fieldName);
		addCondition(fieldName, conditionString, value);
	}

	public LinkedHashMap<String, Object> getParams() {
		return params;
	}

	protected List<String> getConditions() {
		return conditions;
	}

	protected T getKriter() {
		return kriter;
	}

	public String getQueryString() {
		return buildQuery();
	}

	protected Set<String> getJoins() {
		return joins;
	}

	public String[] getParameterNames() {
		if (getParams() == null)
			return null;
		return getParams().keySet().toArray(new String[0]);
	}

	public Object[] getParameterValues() {
		return getParams().values().toArray();
	}
}