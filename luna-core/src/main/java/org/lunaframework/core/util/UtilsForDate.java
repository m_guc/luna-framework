package org.lunaframework.core.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class UtilsForDate {

	public static String datePattern = "dd/MM/yyyy";

	public static String dateTimePattern = "dd/MM/yyyy HH:mm";

	private static SimpleDateFormat dateFormatter = new SimpleDateFormat();
	
	private static final Long BIR_GUN = 1000 * 60 * 60 * 24l;

	public static Date gunEkle(Date tarih, int gunsayisi) {
		long timeinmillis = tarih.getTime() + (gunsayisi * BIR_GUN );
		return new Date(timeinmillis);
	}
	
	/**
	 * 
	 * @param kararBitisTarihi
	 *            Büyük olan tarih
	 * @param kararBaslangicTarihi
	 *            Küçük olan tarih
	 * @return
	 */
	public static int ikiTarihArasiGunSayisi(Date kararBitisTarihi, Date kararBaslangicTarihi) {
		if (kararBaslangicTarihi == null || kararBitisTarihi == null) {
			throw new IllegalArgumentException("KTS-MHK008:Mahkeme kararının tarihleri boş olamaz.");
		}
		int fark = (int) ((kararBitisTarihi.getTime() - kararBaslangicTarihi.getTime()) / BIR_GUN );
		return fark;
	}

	public static String format(Date date) {
		dateFormatter.applyPattern(datePattern);
		return dateFormatter.format(date);
	}

	/**
	 * returns "yyyy/MM/dd" this format
	 * 
	 * @param d
	 * @return
	 */
	public static String yearMonthDayFilePathPattern(Date d) {
		return UtilsForDate.format(d, "yyyy/MM/dd");
	}

	public static String formatDateTime(Date date) {
		dateFormatter.applyPattern(dateTimePattern);
		return dateFormatter.format(date);
	}

	/**
	 * 
	 * @param date
	 * @param pattern
	 *            - Pattern to be applied i.e dd/MM/yyyy
	 * @return
	 */
	public static String format(Date date, String pattern) {
		dateFormatter.applyPattern(pattern);
		return dateFormatter.format(date);
	}

	public static Date parseDate(String dateValue) {
		return parse(dateValue, datePattern);
	}

	public static Date parseDateTime(String dateValue) {
		return parse(dateValue, dateTimePattern);
	}

	public static Date parse(String dateValue, String pattern) {
		dateFormatter.applyPattern(pattern);
		try {
			return dateFormatter.parse(dateValue);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

}
