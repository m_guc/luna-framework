/*
 * Copyright 2008-2009 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.lunaframework.core.util;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.lunaframework.core.model.DateRange;

public class UtilsForReflection {

	private static final Logger logger = Logger.getLogger(UtilsForReflection.class.getCanonicalName());

	public static Method getGetterMethod(Class type, String property) {
		try {
			hasLength(property);
			String name = "get" + StringUtils.capitalize(property);
			Method method = findMethod(type, name, ArrayUtils.EMPTY_CLASS_ARRAY);
			if (method == null) {
				name = "is" + StringUtils.capitalize(property);
				method = findMethod(type, name, ArrayUtils.EMPTY_CLASS_ARRAY);
				if (method == null) {
					throw new RuntimeException("No getter found with isXXX syntax :" + property + " for Class : " + type);
					// logger.warn("No getter found with isXXX syntax :" +
					// property + " for Class : " + type);
				}
			}
			/*
			 * try {
			 * 
			 * getterMethod = classz.getMethod(methodName,null); }
			 * catch(Exception e) { if(logger.isErrorEnabled())logger.error(
			 * "No getter found with getXXX syntax, looking for isXXX syntax :"
			 * + propertyName); try { getterMethod = classz.getMethod("is" +
			 * UtilsForString.capitalize(propertyName),null); } catch
			 * (SecurityException e1) { if(logger.isErrorEnabled())
			 * logger.error(e1.getMessage(), e1); } catch (NoSuchMethodException
			 * e1) { if(logger.isErrorEnabled())
			 * logger.error("No getter found with isXXX syntax :" +
			 * propertyName); throw new RuntimeException(e1); } }
			 */
			return method;
		} catch (Exception e) {
			e.printStackTrace();
			logger.info(e.getMessage());
			// throw new RuntimeException(e);
		}
		return null;
	}

	private static void hasLength(String property) {
		if (property == null || property.trim().length() == 0) {
			throw new RuntimeException("propertyName should have been non empty");
		}
	}

	public static Method getNestedGetterMethod(Class type, String property) {
		// String key = type.getCanonicalName() + "_" + property;
		// if (methodMap.get(key) == null) {
		// methodMap.put(key, getDeepGetterMethod(type, property));
		// }
		// return methodMap.get(key);
		return getDeepGetterMethod(type, property);
	}

	private static Method getDeepGetterMethod(Class type, String property) {
		int index = property.indexOf(".");
		String getterName = index > -1 ? property.substring(0, index) : property;
		Method method = null;
		try {
			hasLength(property);
			String name = "get" + StringUtils.capitalize(getterName);
			method = findMethod(type, name, ArrayUtils.EMPTY_CLASS_ARRAY);
			if (method == null) {
				name = "is" + StringUtils.capitalize(getterName);
				method = findMethod(type, name, ArrayUtils.EMPTY_CLASS_ARRAY);
				if (method == null) {
					logger.log(Level.WARNING, "No getter found with isXXX syntax :" + getterName + " for Class : " + type);
					throw new RuntimeException("No getter found for property :" + getterName + " for Class : " + type);
				}
			}
		} catch (Exception e) {
			logger.log(Level.WARNING, e.getMessage());
		}

		if (index != -1) {
			return getNestedGetterMethod(method.getReturnType(), property.substring(index + 1));
		}
		return method;
	}

	public static Method getNestedSetterMethod(Class type, String property) {
		Method nestedGetterMethod = getNestedGetterMethod(type, property);
		if (nestedGetterMethod == null) {
			throw new RuntimeException(type + " tipinde ['" + property + "'] getter methodu bulunamadı");
		}

		return getNestedSetterMethod(type, property, nestedGetterMethod.getReturnType());
	}

	public static Method getNestedSetterMethod(Class type, String property, Class parameterType) {
		int index = property.indexOf(".");
		String setterName = index > -1 ? property.substring(0, index) : property;
		Method method = null;

		if (index != -1) {
			Method previousGetterMethod2 = getPreviousGetterMethod(type, property);
			return getNestedSetterMethod(previousGetterMethod2.getReturnType(), baseNameOfFieldPath(property));
		}

		try {
			hasLength(property);
			String name = "set" + StringUtils.capitalize(setterName);
			method = findMethod(type, name, new Class[] { parameterType });
			if (method == null) {
				logger.log(Level.WARNING, "No setter found with isXXX syntax :" + setterName + " for Class : " + type);
				// throw new RuntimeException("No setter found for property :" +
				// setterName + " for Class : " + type);
			}
		} catch (Exception e) {
			logger.info(e.getMessage());
		}
		return method;
	}

	private static final Method findMethod(Class type, String name, Class[] parameterTypes) {
		try {
			return type.getMethod(name, parameterTypes);
		} catch (Exception e) {
			return findMethodInObject(name, parameterTypes);
		}
		// Method method = ReflectionUtils.findMethod(type, name,
		// parameterTypes);
		// if (method == null) {
		// method = findMethodInObject(name, parameterTypes);
		// }
		// return method;
	}

	private static final Method findMethodInObject(String name, Class[] parameterTypes) {
		try {
			return Object.class.getDeclaredMethod(name, parameterTypes);
		} catch (Exception ex) {
			return null;
		}
	}

	public static Method getMethod(Class type, String name, Class[] parameterTypes) {
		Method method = findMethod(type, name, parameterTypes);
		if (method == null) {
			logger.info("No method with given name " + name + " found in type " + type);
		}
		return method;
		/*
		 * try { return classz.getMethod(methodName, paramTypes); } catch
		 * (Exception e) { throw new RuntimeException(e); }
		 */
	}

	public static Field getField(Class classz, String fieldPath) {
		hasLength(fieldPath);
		int index = fieldPath.indexOf(".");
		String fieldName = index > -1 ? fieldPath.substring(0, index) : fieldPath;
		Field field = null;

		try {
			field = classz.getDeclaredField(fieldName);
		} catch (SecurityException e) {
			throw new RuntimeException(e);
		} catch (NoSuchFieldException e) {
			classz = classz.getSuperclass();
			if (classz != null) {
				field = getField(classz, fieldName);
			} else {
				throw new IllegalArgumentException(e);
			}
		}

		if (index != -1) {
			return getField(field.getType(), fieldPath.substring(index + 1));
		} else {
			return field;
		}
	}

	public static Field getNestedField(Class classz, String fieldPath) {
		hasLength(fieldPath);

		int index = fieldPath.indexOf(".");
		String fieldName = index > -1 ? fieldPath.substring(0, index) : fieldPath;
		Field field = null;

		try {
			field = classz.getDeclaredField(fieldName);
		} catch (SecurityException e) {
			throw new RuntimeException(e);
		} catch (NoSuchFieldException e) {
			classz = classz.getSuperclass();
			if (classz != null) {
				field = getField(classz, fieldName);
			} else {
				classz = getField(classz, fieldPath.substring(0, fieldPath.indexOf("."))).getClass();
				if (classz != null) {
					field = getField(classz, fieldName);
				} else {
					throw new IllegalArgumentException(e);
				}
			}

		}

		if (index != -1) {
			return getField(field.getType(), fieldPath.substring(index + 1));
		} else {
			return field;
		}
	}

	public static void setValue(Object item, String methodName, Object valueToSet) {
		try {
			Object value = item;
			if (methodName.contains(".")) {
				value = getPreviousInstanceOfProperty(item, methodName);
			}
			UtilsForReflection.getNestedSetterMethod(item.getClass(), methodName).invoke(value, valueToSet);
		} catch (Exception e) {
			e.printStackTrace();
		}
		// if (valueToSet == null) {
		// } else {
		// setValue(item, methodName, valueToSet, valueToSet.getClass());
		// }
	}

	private static void setValue(Object item, String methodName, Object valueToSet, Class valueClass) {
		try {
			// Method nestedSetterMethod =
			// UtilsForReflection.getNestedSetterMethod(item.getClass(),
			// methodName, valueClass);
			Method nestedSetterMethod = UtilsForReflection.getNestedSetterMethod(item.getClass(), methodName);
			nestedSetterMethod.invoke(item, valueToSet);
		} catch (Exception e) {
			e.printStackTrace();
			// FIXME exception yuttuk, cok onemli degil gibi, print
			// edilmeyebilir
			logger.log(Level.WARNING, e.getMessage());
			// e.printStackTrace();
		}
	}

	public static Method getSetterMethod(Class type, String property, Class parameterType) {
		hasLength(property);
		String name = "set" + StringUtils.capitalize(property);
		Method method = findMethod(type, name, new Class[] { parameterType });
		if (method == null) {
			logger.log(Level.WARNING, "No setter found for " + property + " in type " + type);
			// throw new RuntimeException("No setter found for " + property +
			// " in type " + type);
		}
		return method;
		/*
		 * try { Assert.hasLength(property,
		 * "property should have been non empty"); String name = "set" +
		 * UtilsForString.capitalize(property); Method method =
		 * type.getMethod(name,new Class[]{parameterTypes}); return method; }
		 * catch(Exception e) { throw new RuntimeException(e); }
		 */
	}

	public static boolean isGetterMethod(Class clazz, Method method) {
		return (getGetterMethod(clazz, method.getName()) != null);
	}

	public static Object getValue(Object obj, String fieldPath) {
		if (obj == null)
			return null;

		if (fieldPath.contains("[")) {
			return getValueByExpression(obj, fieldPath, "");
		}

		if (fieldPath.contains(":")) {
			String[] split = fieldPath.split(":");
			fieldPath = split[0];
			String dateFormat = split[1];

			if (dateFormat != null && !dateFormat.isEmpty()) {
				Object value = getValue(obj, fieldPath);
				if (value instanceof Date) {
					return UtilsForDate.format((Date) value, dateFormat);
				}
				return value;
			}
		}

		Object value = null;
		hasLength(fieldPath);

		int index = fieldPath.indexOf(".");
		String field = index > -1 ? fieldPath.substring(0, index) : fieldPath;
		Object fieldValue = getFieldValue(obj, field);

		if (index != -1 && fieldValue != null) {
			value = getValue(fieldValue, fieldPath.substring(index + 1));
		} else {
			value = fieldValue;
		}

		return value;
	}

	public static String getValueByExpression(Object obj, String expression) {
		return getValueByExpression(obj, expression, "");
	}

	public static String getValueByExpression(final Object obj, String expression, final String nullExpression) {
		return getValueByExpression(obj, expression, nullExpression, new PropertyToStringConverter() {
			@Override
			public String toString(Object value, String fieldName) {
				return value == null ? nullExpression : value.toString();
			}
		});
	}

	public static String getValueByExpression(Object obj, String expression, PropertyToStringConverter toStringConverter) {
		return getValueByExpression(obj, expression, "", toStringConverter);
	}

	public static String getValueByExpression(Object obj, String expression, String nullExpression, PropertyToStringConverter toStringConverter) {
		if (obj == null)
			return "";
		hasLength(expression);

		if (!expression.contains("[") && !expression.contains("]")) {
			Object value = getValue(obj, expression);
			return String.valueOf(value == null ? nullExpression : value);
		}

		String pattern = "\\[.*?]";
		String labelDefinition = expression;

		String[] arr = labelDefinition.split(pattern);
		Matcher matcher = Pattern.compile(pattern).matcher(labelDefinition);

		int i = 0;
		StringBuilder builder = new StringBuilder();
		while (matcher.find()) {
			int start = matcher.start();
			int end = matcher.end();

			String delimeter = arr.length == 0 ? "" : arr[i];
			String fieldPath = labelDefinition.substring(start + 1, end - 1);

			Object fieldValue = getValue(obj, fieldPath);
			builder.append(delimeter);

			if (toStringConverter != null) {
				fieldValue = toStringConverter.toString(fieldValue, fieldPath);
			}
			builder.append(fieldValue == null ? nullExpression : fieldValue);
			i++;
		}
		return builder.toString();
	}

	private static Object getFieldValue(Object obj, String fieldName) {
		if (obj == null)
			return null;

		if (obj instanceof Map) {
			return ((Map) obj).get(fieldName);
		}

		if (obj instanceof Collection) {
			Collection collection = (Collection) obj;
			LinkedHashSet items = new LinkedHashSet();
			for (Object object : collection) {
				items.add(getValue(object, fieldName));
			}
			return items;
		}

		try {
			Method method = null;
			try {
				method = getGetterMethod(obj.getClass(), fieldName);
			} catch (Exception ex) {
				method = getMethod(obj.getClass(), fieldName, null);
			}

			if (method == null)
				return null;
			return method.invoke(obj, null);
		} catch (Exception e) {
			logger.log(Level.WARNING, "object : " + obj + " fieldName : " + fieldName + " " + e);
		}
		return null;
	}

	public static Collection<?> collectProperty(Collection<?> collection, String propertyName) {
		if (collection == null) {
			return null;
		}

		List<Object> items = new ArrayList<Object>();
		for (Object object : collection) {
			items.add(getValue(object, propertyName));
		}
		return items;
	}

	public static List<Field> getAllFields(Class type) {
		List<Field> fields = new LinkedList<Field>();
		for (Field field : type.getDeclaredFields()) {
			fields.add(field);
		}
		if (type.getSuperclass() != null) {
			fields.addAll(getAllFields(type.getSuperclass()));
		}
		return fields;
	}

	public static List<Method> getGetterMethods(Class clazz) {
		LinkedList<Method> getterMethods = new LinkedList<Method>();

		// for (Method method : ReflectionUtils.getAllDeclaredMethods(clazz)) {

		for (Method method : getAllDeclaredMethods(clazz)) {
			String methodName = method.getName();
			if (methodName.startsWith("get") || methodName.startsWith("is")) {

				if (methodName.replace("get", "").toLowerCase().equals("class")) {
					continue;
				}

				getterMethods.add(method);
			}
		}
		return getterMethods;
	}

	public static Collection<String> getFieldNames(Class clazz) {
		List<Field> fields = getAllFields(clazz);
		Set<String> names = new LinkedHashSet<String>();
		for (Field field : fields) {
			names.add(field.getName());
		}
		return names;
	}

	public static List<String> getFieldNamesByGetterMethod(Class clazz) {
		List<Method> getters = getGetterMethods(clazz);
		List<String> names = new LinkedList<String>();
		for (Method method : getters) {
			String propertyName = null;
			if (method.getName().startsWith("is")) {
				propertyName = StringUtils.replaceOnce(method.getName(), "is", "");
			} else {
				propertyName = StringUtils.replaceOnce(method.getName(), "get", "");
			}
			names.add(StringUtils.uncapitalize(propertyName));
		}
		return names;
	}

	public static Object getValueOfField(Object obj, Integer fieldIndex) {
		return getValueOfField(obj, fieldIndex, null);
	}

	public static Object getValueOfField(Object obj, String fieldName) {
		return getValueOfField(obj, 0, fieldName);
	}

	public static Object getValueOfField(Object obj, Integer fieldIndex, String fieldName) {
		Object value = null;
		if (obj instanceof Map) {
			if (fieldName == null) {
				throw new IllegalArgumentException(obj.getClass() + " tipindeki bir nesnenin değerlerini alabilmek için field ismi belirtmelisiniz !");
			}
			value = ((Map) obj).get(fieldName);
		} else if (obj instanceof Object[]) {
			value = ((Object[]) obj)[fieldIndex];
		} else if (obj instanceof List) {
			value = ((List) obj).get(fieldIndex);
		} else {
			if (fieldName == null) {
				throw new IllegalArgumentException(obj.getClass() + " tipindeki bir nesnenin değerlerini alabilmek için field ismi belirtmelisiniz !");
			}
			value = getValue(obj, fieldName);
		}
		return value;
	}

	public static String baseNameOfGetterMethod(String name) {
		return StringUtils.uncapitalize(StringUtils.replaceOnce(name, "get", ""));
	}

	public static String baseNameOfFieldPath(String fieldPath) {
		return fieldPath.substring(fieldPath.lastIndexOf(".") + 1);
	}

	public static String getPreviousPathOfProperty(String fullPath, String property) {
		StringBuilder b = new StringBuilder(fullPath);
		b.delete(fullPath.lastIndexOf("." + property), b.capacity());
		return b.toString();
	}

	public static Object getPreviousInstanceOfProperty(Object object, String path) {
		return getPreviousInstanceOfProperty(object, path, baseNameOfFieldPath(path));
	}

	public static Object getPreviousInstanceOfProperty(Object object, String fullPath, String property) {
		return UtilsForReflection.getValue(object, getPreviousPathOfProperty(fullPath, property));
	}

	public static Class getPreviousClassOfProperty(Class clazz, String fullPath, String property) {
		String path = getPreviousPathOfProperty(fullPath, property);
		return UtilsForReflection.getNestedGetterMethod(clazz, path).getReturnType();
	}

	public static Method getPreviousGetterMethod(Class parameterType, String property) {
		String baseNameOfFieldPath = UtilsForReflection.baseNameOfFieldPath(property);
		return getNestedGetterMethod(parameterType, property.replace("." + baseNameOfFieldPath, ""));
	}

	public static Class getGenericTypeOfProperty(Class clazz, String propertyName) {
		Type[] types = getGenericTypesOfProperty(clazz, propertyName);
		if (types.length > 0)
			return (Class) types[0];
		return null;
	}

	public static Type[] getGenericTypes(Class<?> clz) {
		return clz.getGenericInterfaces();
	}

	public static Type[] getGenericTypesOfProperty(Class clazz, String propertyName) {
		return ((ParameterizedType) getNestedGetterMethod(clazz, propertyName).getGenericReturnType()).getActualTypeArguments();
	}

	public static interface PropertyToStringConverter {

		public String toString(Object value, String fieldName);
	}

	private static List<Method> getAllDeclaredMethods(Class clazz) {
		ArrayList<Method> methodList = new ArrayList<Method>();
		Method[] methods = clazz.getDeclaredMethods();
		for (Method method : methods) {
			methodList.add(method);
		}

		if (clazz.getSuperclass() != null) {
			methodList.addAll(getAllDeclaredMethods(clazz.getSuperclass()));
		} else if (clazz.isInterface()) {
			for (Class<?> superIfc : clazz.getInterfaces()) {
				methodList.addAll(getAllDeclaredMethods(superIfc));
			}
		}

		return methodList;
	}

	public static void main(String[] args) {
		Method nestedGetterMethod = getNestedGetterMethod(DateRange.class, "lowerBound.time");
		System.out.println(nestedGetterMethod);
	}

}
