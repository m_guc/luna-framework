/*
 * Copyright 2008-2009 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.lunaframework.core.util;

import java.awt.Image;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.lunaframework.core.model.Identity;
import org.lunaframework.core.model.ToString;
import org.springframework.core.annotation.AnnotationUtils;

import sun.awt.image.ToolkitImage;

/**
 * @author elcezeri
 * @since 1.0
 */
public class UtilsForObject {

	public static int compareTo(Object thisObj, Object thatObj) {
		// Assert.notNull(thisObj,
		// "Source object for compareTo cannot be null");
		if (thatObj == null) {
			return 1;
		} else if (!getClass(thisObj).isAssignableFrom(getClass(thatObj))) {
			throw new ClassCastException(getClass(thatObj) + " is not assignable to " + getClass(thisObj));
		} else {
			String[] properties = getIdentityKeysFromCache(getClass(thisObj));
			CompareToBuilder compareToBuilder = new CompareToBuilder();

			for (String property : properties) {
				if (property.length() == 0)
					continue;
				Object valueOfThis = UtilsForReflection.getValue(thisObj, property);
				Object valueOfObj = UtilsForReflection.getValue(thatObj, property);
				compareToBuilder = compareToBuilder.append(valueOfThis, valueOfObj);
			}
			return compareToBuilder.toComparison();
		}
	}

	public static String toString(Object thisObj, String defaultValue) {
		ToStringConfig config = getToStringConfigFromCache(getClass(thisObj));
		String[] properties = config.properties;
		ToStringStyle toStringStyle = config.toStringStyle;
		if (properties.length == 0) {
			return defaultValue;
		} else {
			ToStringBuilder toStringBuilder = new ToStringBuilder(thisObj, toStringStyle);
			for (String property : properties) {
				if (property.length() == 0)
					continue;
				Object value = UtilsForReflection.getValue(thisObj, property);
				toStringBuilder.append(property, value);
			}
			return toStringBuilder.toString();
		}
	}

	public static boolean equals(Object thisObj, Object obj) {
		if (obj != null && getClass(thisObj).isAssignableFrom(getClass(obj))) {

			String[] properties = getIdentityKeysFromCache(getClass(thisObj));

			if (properties.length == 0) {
				return false;
			} else {
				EqualsBuilder equalsBuilder = new EqualsBuilder();

				for (String property : properties) {
					if (property.length() == 0)
						continue;
					Object valueOfThis = UtilsForReflection.getValue(thisObj, property);
					Object valueOfObj = UtilsForReflection.getValue(obj, property);
					equalsBuilder = equalsBuilder.append(valueOfThis, valueOfObj);
				}
				return equalsBuilder.isEquals();
			}
		} else {
			return false;
		}
	}

	public static int hashCode(Object thisObj) {
		String[] properties = getIdentityKeysFromCache(getClass(thisObj));

		if (properties.length == 0) {
			return 0;
		} else {
			HashCodeBuilder hashCodeBuilder = new HashCodeBuilder();
			for (String property : properties) {
				if (property.length() == 0)
					continue;
				Object valueOfThis = UtilsForReflection.getValue(thisObj, property);
				hashCodeBuilder.append(valueOfThis);
			}
			return hashCodeBuilder.toHashCode();
		}
	}

	public static final String[] getIdentityKeys(Class classz) {
		if (classz == null) {
			return ArrayUtils.EMPTY_STRING_ARRAY;
		} else {
			Identity keySet = AnnotationUtils.isAnnotationDeclaredLocally(Identity.class, classz) ? AnnotationUtils.findAnnotation(classz, Identity.class)
					: null;
			if (keySet == null) {
				return getIdentityKeys(classz.getSuperclass());
			} else {
				if (keySet.inherit()) {
					return (String[]) ArrayUtils.addAll(getIdentityKeys(classz.getSuperclass()), keySet.values());
				} else
					return keySet.values();
			}
		}
	}

	public static Object[] getIdentityValues(Object o) {
		// Assert.notNull(o, "Object cannot be null");
		String[] properties = getIdentityKeys(getClass(o));

		if (properties.length == 0) {
			return ArrayUtils.EMPTY_OBJECT_ARRAY;
		} else {
			List<Object> values = new ArrayList<Object>();
			for (String property : properties) {
				if (property.length() == 0)
					continue;
				Object propValue = UtilsForReflection.getValue(o, property);
				values.add(propValue);
			}
			return values.toArray();
		}
	}

	public static Object serializeDeserialize(Object obj) {
		ObjectInputStream in = null;
		ObjectOutputStream out = null;
		try {
			ByteArrayOutputStream bout = new ByteArrayOutputStream();
			out = new ObjectOutputStream(bout);
			out.writeObject(obj);
			ByteArrayInputStream bin = new ByteArrayInputStream(bout.toByteArray());
			in = new ObjectInputStream(bin);
			obj = in.readObject();
			return obj;
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		} finally {
			try {
				out.close();
			} catch (Exception ex) {
			}
			try {
				in.close();
			} catch (Exception ex) {
			}
		}
	}

	/**
	 * TODO: ExceptionUtils'e cekilmeli Find the root cause of given throwable.
	 * For use on JDK 1.4 or later.
	 */
	public static Throwable findRootCause(Throwable e) {
		Throwable cause = e.getCause();
		if (cause == null) {
			return e;
		} else {
			return findRootCause(cause);
		}
	}

	private static class ToStringConfig {
		public String[] properties;
		public ToStringStyle toStringStyle;

		public ToStringConfig(String[] properties, ToStringStyle toStringStyle) {
			this.properties = properties;
			this.toStringStyle = toStringStyle;
		}
	}

	private static class IdentityConfig {
		public String[] properties;

		public IdentityConfig(String[] properties) {
			this.properties = properties;
		}
	}

	private static final Map<Class, IdentityConfig> identityCache = new HashMap<Class, IdentityConfig>();
	private static final Map<Class, ToStringConfig> toStringCache = new HashMap<Class, ToStringConfig>();

	private static synchronized final String[] getIdentityKeysFromCache(Class type) {
		String[] properties = null;
		IdentityConfig config = identityCache.get(type);
		if (config == null) {
			properties = getIdentityKeys(type);
			identityCache.put(type, new IdentityConfig(properties));
		} else {
			properties = config.properties;
		}
		return properties;
	}

	private static synchronized final ToStringConfig getToStringConfigFromCache(Class type) {
		ToStringConfig config = toStringCache.get(type);
		if (config == null) {
			String[] properties = getToStringAttributes(type);
			ToStringStyle toStringStyle = getToStringStyle(type);
			config = new ToStringConfig(properties, toStringStyle);
			toStringCache.put(type, config);
		}
		return config;
	}

	private static final String[] getToStringAttributes(Class classz) {
		if (classz == null) {
			return ArrayUtils.EMPTY_STRING_ARRAY;
		} else {
			ToString toString = AnnotationUtils.isAnnotationDeclaredLocally(ToString.class, classz) ? AnnotationUtils.findAnnotation(classz, ToString.class)
					: null;
			if (toString == null) {
				return getToStringAttributes(classz.getSuperclass());
			} else {
				if (toString.inherit()) {
					return (String[]) ArrayUtils.addAll(toString.values(), getToStringAttributes(classz.getSuperclass()));
				} else {
					return toString.values();
				}
			}
		}
	}

	private static final ToStringStyle getToStringStyle(Class classz) {
		if (classz == null) {
			return ToStringStyle.DEFAULT_STYLE;
		} else {
			ToString toString = AnnotationUtils.findAnnotation(classz, ToString.class);
			if (toString != null) {
				return toString.style().getToStringStyle();
			} else {
				return getToStringStyle(classz.getSuperclass());
			}
		}
	}

	private static final Class getClass(Object obj) {
		return obj.getClass();
		// return Hibernate.getClass(obj);
	}

	public static byte[] getBytesOfImage(ImageIcon imageIcon) {
		return getBytesOfImage(imageIcon.getImage());
	}

	public static byte[] getBytesOfImage(Image image) {
		if (image == null) {
			return null;
		}

		if (!(image instanceof ToolkitImage)) {
			return null;
		}

		ByteArrayOutputStream baos = new ByteArrayOutputStream();

		try {
			ImageIO.write(((ToolkitImage) image).getBufferedImage(), "png", baos);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return baos.toByteArray();
	}
}
