package org.lunaframework.core.util;

import java.util.Collection;
import java.util.LinkedHashSet;

public class UtilsForCollections {

	public static <T extends Object> Collection<T> toCollection(T[] objects) {
		LinkedHashSet<T> set = new LinkedHashSet<T>(objects.length);
		for (T object : objects) {
			set.add(object);
		}
		return set;
	}

	public static String toString(Collection paramObject, String property) {
		if (paramObject == null) {
			return "[]";
		}

		Collection col = (Collection) paramObject;
		if (col.size() == 0) {
			return "[]";
		}

		int i = 0;
		LinkedHashSet<String> collection = new LinkedHashSet<String>();
		// String result = "[";
		for (Object object : col) {
			Object value = property != null ? UtilsForReflection.getValue(object, property) : object;
			collection.add(String.valueOf(value));
			// result += value;
			if (i >= 4) {
				collection.add(" ... ");
				break;
				// return result + " ... ]";
			}
			i++;
		}

		return "[ " + join(collection, " , ") + " ]";
	}

	private static String join(Collection<String> collection, String seperator) {
		StringBuilder builder = new StringBuilder();
		int i = 0;
		for (String value : collection) {
			builder.append(value);
			if (i != collection.size() - 1) {
				builder.append(seperator);
			}
			i++;
		}
		return builder.toString();
	}

}
