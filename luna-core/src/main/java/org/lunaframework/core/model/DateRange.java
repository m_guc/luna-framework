package org.lunaframework.core.model;

import java.util.Date;

import org.apache.commons.lang.time.DateUtils;
import org.lunaframework.core.util.UtilsForDate;


public class DateRange extends AbstractEntity {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Date lowerBound = getLowerDefaultValue();
	private Date upperBound = new Date();

	public DateRange() {

	}

	public DateRange(Date lowerBound, Date upperBound) {
		setLowerBound(lowerBound);
		setUpperBound(upperBound);
	}

	public Date getLowerBound() {
		return lowerBound;
	}

	public void setLowerBound(Date lowerBound) {
		this.lowerBound = lowerBound;
	}

	public Date getUpperBound() {
		return upperBound;
	}

	public void setUpperBound(Date upperBound) {
		this.upperBound = upperBound;
	}

	private Date getLowerDefaultValue() {
		return DateUtils.addDays(new Date(), -30);
	}

	@Override
	public String toString() {
		return "Tarih Aralığı [" + (lowerBound != null ? "tarih1=" + UtilsForDate.format(lowerBound) + ", " : "")
				+ (upperBound != null ? "tarih1=" + UtilsForDate.format(upperBound) : "") + "]";
	}

	public static void main(String[] args) {

		DateRange dateRange = new DateRange();
		System.out.println(dateRange);

	}
}
