package org.lunaframework.core.model;

@Identity(values = { "data", "name", "dataSize", "extension" })
@ToString(values = { "name", "dataSize" ,"filePath"})
public class InMemoryFile extends AbstractEntity {

	private transient String filePath;
	private byte[] data;
	private String name;
	private String contentType;
	private Long dataSize;

	public InMemoryFile() {
	}

	public InMemoryFile(String name) {
		this(name, null);
	}

	public InMemoryFile(String name, String contentType) {
		this(name, contentType, null);
	}

	public InMemoryFile(String name, String contentType, String filePath) {
		this(name, contentType, filePath, null);
	}

	public InMemoryFile(String name, String contentType, String filePath, byte[] data) {
		this.name = name;
		this.contentType = contentType;
		this.filePath = filePath;
		setData(data);
	}

	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
		if (data != null) {
			this.dataSize = ((Integer) this.data.length).longValue();
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public Long getDataSize() {
		return dataSize;
	}
}
