package org.lunaframework.core.model;

import java.io.Serializable;

import org.lunaframework.core.util.UtilsForObject;


public abstract class AbstractEntity implements Serializable {

	public boolean equals(Object obj) {
		return UtilsForObject.equals(this, obj);
	}

	public int hashCode() {
		return UtilsForObject.hashCode(this);
	}

	public String toString() {
		return UtilsForObject.toString(this, super.toString());
	}
	
}
