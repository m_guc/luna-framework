package org.lunaframework.core.model;

import java.util.LinkedHashMap;
import java.util.Map;

public class TwinMap<K, V> extends LinkedHashMap<K, V> {

	private Map<V, K> inverseMap = new LinkedHashMap<V, K>();

	@Override
	public V put(K key, V value) {
		inverseMap.put(value, key);
		return super.put(key, value);
	}

	@Override
	public void putAll(Map<? extends K, ? extends V> m) {
		for (K k : m.keySet()) {
			inverseMap.put(m.get(k), k);
		}
		super.putAll(m);
	}

	public K getKey(V value) {
		return inverseMap.get(value);
	}
	
	@Override
	public void clear() {
		inverseMap.clear();
		super.clear();
	}
}
