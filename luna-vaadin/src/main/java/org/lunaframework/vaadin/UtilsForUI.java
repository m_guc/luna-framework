package org.lunaframework.vaadin;

import com.vaadin.server.ExternalResource;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickListener;

public class UtilsForUI {

	public static Button createButton(String caption, ClickListener clickListener) {
		return createButton(caption, clickListener, null);
	}

	public static Button createButton(String caption, ClickListener clickListener, String imagePath) {
		Button button = new Button(caption, clickListener);
		if (imagePath != null) {
			button.setIcon(new ExternalResource(imagePath));
		}
		return button;
	}
}
