package org.lunaframework.vaadin.view;

import com.vaadin.navigator.View;

public class ViewMetaData {

	private String name;
	private String title;
	private Class<? extends View> viewClass;
	private String iconPath;
	private String styleName;
	private LoadStrategy loadStrategy;
	private String[] restrictions;

	public ViewMetaData(String name, String title, Class viewClass) {
		this(name, title, viewClass, null);
	}

	public ViewMetaData(String name, String title, Class viewClass, String iconPath) {
		this(name, title, viewClass, iconPath, LoadStrategy.LAZY);
	}

	public ViewMetaData(String name, String title, Class viewClass, String iconPath, LoadStrategy loadStrategy) {
		super();
		this.name = name;
		this.title = title;
		this.viewClass = viewClass;
		this.iconPath = iconPath;
		this.loadStrategy = loadStrategy;
	}

	public String getName() {
		return name;
	}

	public String getTitle() {
		return title;
	}

	public Class<? extends View> getViewClass() {
		return viewClass;
	}

	public String getIconPath() {
		return iconPath;
	}

	public void setIconPath(String iconPath) {
		this.iconPath = iconPath;
	}

	public String getStyleName() {
		return styleName;
	}

	public void setStyleName(String styleName) {
		this.styleName = styleName;
	}

	public LoadStrategy getLoadStrategy() {
		return loadStrategy;
	}

	public void setLoadStrategy(LoadStrategy loadStrategy) {
		this.loadStrategy = loadStrategy;
	}

	public String[] getRestrictions() {
		return restrictions;
	}

	public void setRestrictions(String[] restrictions) {
		this.restrictions = restrictions;
	}

	public boolean isRestricted() {
		return getRestrictions().length > 0;
	}

}
