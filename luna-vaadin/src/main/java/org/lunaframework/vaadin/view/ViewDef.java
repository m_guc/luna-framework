package org.lunaframework.vaadin.view;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface ViewDef {

	public abstract String name();

	public abstract String title();

	public String iconPath() default "";

	public String styleName() default "";

	public int priority() default 0;

	public LoadStrategy loadStrategy() default LoadStrategy.LAZY;

	public String[] restrictions() default {};
}
