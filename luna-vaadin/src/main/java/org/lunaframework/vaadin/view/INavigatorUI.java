package org.lunaframework.vaadin.view;

public interface INavigatorUI {

	public void addView(ViewMetaData viewMetaData);

	public void viewSelected(ViewMetaData viewMetaData);
}
