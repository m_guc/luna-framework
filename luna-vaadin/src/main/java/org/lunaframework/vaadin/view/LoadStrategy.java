package org.lunaframework.vaadin.view;

public enum LoadStrategy {
	EAGER, LAZY, ALWAYS_CREATE_NEW;
}
