package org.lunaframework.vaadin.view;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.lunaframework.core.util.UtilsForPackageScan;

import com.vaadin.annotations.Title;
import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewDisplay;
import com.vaadin.navigator.ViewProvider;
import com.vaadin.server.Page;
import com.vaadin.ui.ComponentContainer;
import com.vaadin.ui.UI;

public class ViewNavigator extends Navigator {

	private Map<String, ViewMetaData> viewMetadataMap = new LinkedHashMap<String, ViewMetaData>();
	private INavigatorUI navigatorUI;
	private String welcomeView;

	private Set<String> roles = new HashSet<String>();

	public ViewNavigator(UI ui, ComponentContainer container) {
		this(ui, container, null);
	}

	public ViewNavigator(UI ui, ComponentContainer container, INavigatorUI navigatorUI) {
		this(ui, new ComponentContainerViewDisplay(container), navigatorUI);
		this.navigatorUI = navigatorUI;
	}

	public ViewNavigator(UI ui, ViewDisplay viewDisplay, INavigatorUI navigatorUI) {
		super(ui, viewDisplay);
		this.navigatorUI = navigatorUI;
	}

	public void createViewsFromPackage(String packageName) {
		buildViewMetaDataInstances(packageName);
		createViewProviders();
	}

	private void createViewProviders() {
		for (String name : viewMetadataMap.keySet()) {
			ViewMetaData viewMetaData = viewMetadataMap.get(name);
			if (viewMetaData.isRestricted()) {
				for (String role : viewMetaData.getRestrictions()) {
					if (role != null && role.trim().length() > 0) {
						if (hasRole(role)) {
							addViewProvider(name, viewMetaData);
						}
					}
				}
			} else {
				addViewProvider(name, viewMetaData);
			}
		}
	}

	private void addViewProvider(String viewName, ViewMetaData viewMetaData) {
		addProvider(createViewProvider(viewMetaData));
		if (navigatorUI != null) {
			this.navigatorUI.addView(viewMetaData);
		}
	}

	private ViewMetaData createViewMetaData(Class<? extends View> viewClass) {
		ViewDef viewDef = viewClass.getAnnotation(ViewDef.class);
		String name = viewDef.name();
		return createViewMetaData(name, viewClass);
	}

	private ViewMetaData createViewMetaData(String viewName, Class<? extends View> viewClass) {
		ViewDef viewDef = viewClass.getAnnotation(ViewDef.class);
		String title = viewDef.title();
		LoadStrategy loadStrategy = viewDef.loadStrategy();
		String iconPath = viewDef.iconPath();
		ViewMetaData viewMetaData = new ViewMetaData(viewName, title, viewClass, iconPath, loadStrategy);
		viewMetaData.setStyleName(viewDef.styleName());
		viewMetaData.setRestrictions(viewDef.restrictions());
		return viewMetaData;
	}

	@Override
	protected void navigateTo(View view, String viewName, String parameters) {
		super.navigateTo(view, viewName, parameters);
		ViewMetaData viewMetaData = viewMetadataMap.get(viewName);
		if (viewMetaData != null) {
			setPageTitle(viewMetaData);
			if (navigatorUI != null) {
				navigatorUI.viewSelected(viewMetaData);
			}
		}
	}

	private void setPageTitle(ViewMetaData viewMetaData) {
		StringBuilder title = new StringBuilder();
		Title annotation = getUI().getClass().getAnnotation(Title.class);
		if (annotation != null) {
			title.append(annotation.value());
			title.append(" - ");
		}
		title.append(viewMetaData.getTitle());
		Page.getCurrent().setTitle(title.toString());
	}

	private ViewProvider createViewProvider(ViewMetaData viewMetaData) {
		switch (viewMetaData.getLoadStrategy()) {
		case EAGER:
			return new StaticViewProvider(viewMetaData.getName(), createView(viewMetaData.getViewClass()));
		case LAZY:
			return new ClassBasedLazyViewProvider(viewMetaData.getName(), viewMetaData.getViewClass());
		case ALWAYS_CREATE_NEW:
			return new ClassBasedViewProvider(viewMetaData.getName(), viewMetaData.getViewClass());
		default:
			return new ClassBasedLazyViewProvider(viewMetaData.getName(), viewMetaData.getViewClass());
		}
	}

	private View createView(Class<? extends View> clazz) {
		try {
			return clazz.newInstance();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	private void buildViewMetaDataInstances(String packageName) {
		List<Class> list = new ArrayList<Class>(UtilsForPackageScan.getAnnotatedClasses(packageName, ViewDef.class));

		Collections.sort(list, new Comparator<Class>() {
			@Override
			public int compare(Class o1, Class o2) {
				ViewDef annotation1 = (ViewDef) o1.getAnnotation(ViewDef.class);
				ViewDef annotation2 = (ViewDef) o2.getAnnotation(ViewDef.class);
				return ((Integer) annotation1.priority()).compareTo(annotation2.priority());
			}
		});

		for (Class<? extends View> clazz : list) {
			ViewMetaData viewMetaData = createViewMetaData(clazz);
			viewMetadataMap.put(viewMetaData.getName(), viewMetaData);
		}
	}

	/**
	 * Should be set after createViewsFromPackage if used
	 * 
	 * @param welcomeView
	 */
	public void setWelcomeView(String welcomeView) {
		this.welcomeView = welcomeView;
		ViewMetaData viewMetaData = viewMetadataMap.get(welcomeView);
		if (viewMetaData != null) {
			this.addView("", viewMetaData.getViewClass());
			viewMetadataMap.put("", viewMetaData);
		}
	}

	public void addRole(String role) {
		roles.add(role);
	}

	public boolean hasRole(String role) {
		return roles.contains(role);
	}

}
