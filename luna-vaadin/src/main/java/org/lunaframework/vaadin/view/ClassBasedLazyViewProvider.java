package org.lunaframework.vaadin.view;

import com.vaadin.navigator.Navigator.ClassBasedViewProvider;
import com.vaadin.navigator.View;

public class ClassBasedLazyViewProvider extends ClassBasedViewProvider {

	private View view;

	public ClassBasedLazyViewProvider(String viewName, Class<? extends View> arg1) {
		super(viewName, arg1);
	}

	@Override
	public View getView(String viewName) {
		if (view == null) {
			view = super.getView(viewName);
		}
		return view;
	}

}
