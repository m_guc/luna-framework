package org.lunaframework.vaadin.panel;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.vaadin.ui.Component;

public class ContentPanelManager implements Serializable {

	private static final long serialVersionUID = 1L;

	private Map<Class<? extends Component>, Component> widgetMap = new HashMap<Class<? extends Component>, Component>();

	public Component getPanel(Class<? extends Component> componentClass) {
		return createPanel(componentClass, false);
	}

	public Component createPanel(Class<? extends Component> componentClass, boolean createNew) {
		if (createNew) {
			return createWidget(componentClass);
		} else {
			Component widget = widgetMap.get(componentClass);
			if (widget == null) {
				widget = createWidget(componentClass);
				widgetMap.put(componentClass, widget);
			}
			return widget;
		}
	}

	private <T extends Component> T createWidget(Class<? extends Component> componentClass) {
		try {
			return (T) componentClass.newInstance();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
