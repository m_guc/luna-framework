package org.lunaframework.vaadin.mediator;

import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class Mediator {

	// event listeners
	private Set<MediatorEventHandler> handlerList = new LinkedHashSet<MediatorEventHandler>();

	private List<MediatorEvent> eventQueue = new LinkedList<MediatorEvent>();
	private boolean eventFired = false;
	private int queueIndex = 0;

	public void addMediatorEventListener(MediatorEventHandler... listeners) {
		for (MediatorEventHandler mediatorEventListener : listeners) {
			addMediatorEventHandler(mediatorEventListener);
		}
	}

	public void addMediatorEventHandler(MediatorEventHandler listener) {
		handlerList.add(listener);
	}

	public void removeMediatorEventHandler(MediatorEventHandler listener) {
		handlerList.remove(listener);
	}

	public void fire(MediatorEvent event) {
		if (!eventFired) {
			try {
				eventFired = true;
				fireEvent(event);

				while (!eventQueue.isEmpty()) {
					queueIndex = 0;
					fireEvent((MediatorEvent) eventQueue.remove(0));
				}
			} finally {
				eventFired = false;
			}
		} else {
			queueEvent(event);
		}
	}

	private void queueEvent(MediatorEvent event) {
		eventQueue.add(queueIndex++, event);
	}

	private void fireEvent(MediatorEvent event) {
		MediatorEventHandler[] snapshot = handlerList.toArray(new MediatorEventHandler[0]);
		for (MediatorEventHandler handler : snapshot) {
			if (handlerList.contains(handler)) {
				event.dispatch(handler);
			}
		}

	}
}
