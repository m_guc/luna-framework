package org.lunaframework.vaadin.mediator;

import org.lunaframework.core.util.UtilsForReflection;

import com.vaadin.ui.UI;

public abstract class MediatorEvent {

	public void fire() {
		Object value = UtilsForReflection.getValue(UI.getCurrent(), "mediator");
		((Mediator) value).fire(this);
	}

	public abstract void dispatch(MediatorEventHandler handler);

}