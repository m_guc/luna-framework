package org.lunaframework.vaadin.component.grid;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;

import org.lunaframework.core.util.UtilsForReflection;
import org.lunaframework.vaadin.component.PropertyDescriptor;

import com.vaadin.data.Container;
import com.vaadin.data.Container.Filterable;
import com.vaadin.data.Container.Sortable;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.Property.ValueChangeNotifier;
import com.vaadin.data.util.AbstractInMemoryContainer;
import com.vaadin.data.util.NestedMethodProperty;
import com.vaadin.data.util.filter.UnsupportedFilterException;

public class SimpleBeanContainer<T> extends AbstractInMemoryContainer<Object, NestedMethodProperty, SimpleBeanItem<T>> implements ValueChangeNotifier,
		ValueChangeListener, Container.Ordered, Filterable, Sortable {

	// property descriptor map for creating bean item properties
	Map<String, PropertyDescriptor> propertyDescriptorMap;

	// itemId:entityItem container
	private LinkedHashMap<Object, SimpleBeanItem<T>> itemIdBeanItemMap = new LinkedHashMap<Object, SimpleBeanItem<T>>();

	// entity:entityItem container, for performance reasons
	private LinkedHashMap<Object, SimpleBeanItem<T>> entityMap = new LinkedHashMap<Object, SimpleBeanItem<T>>();

	private Class<T> clazz;
	private String idFieldName;

	public SimpleBeanContainer(String idFieldName, Class<T> clazz, String... propertyNames) {
		this(idFieldName, clazz, null, propertyNames);
	}

	public SimpleBeanContainer(String idFieldName, Class<T> clazz, Class[] propertyTypes, String... propertyNames) {
		this.idFieldName = idFieldName;
		this.clazz = clazz;
		this.propertyDescriptorMap = propertyTypes != null ? PropertyDescriptorUtility.createPropertyDescriptors(propertyTypes, propertyNames)
				: PropertyDescriptorUtility.createPropertyDescriptorsByClazz(clazz, propertyNames);
	}

	private Object getIdValue(T entity) {
		return UtilsForReflection.getValue(entity, idFieldName);
	}

	public SimpleBeanItem<T> addBean(T entity) {
		SimpleBeanItem<T> item = createBeanItem(entity);
		return internalAddItemAtEnd(item.getItemId(), item, true);
	}

	public SimpleBeanItem<T> addBeanAt(T entity, int index) {
		SimpleBeanItem<T> item = createBeanItem(entity);
		return internalAddItemAt(index, item.getItemId(), item, true);
	}

	private SimpleBeanItem<T> createBeanItem(T entity) {
		return new SimpleBeanItem<T>(getIdValue(entity), entity, propertyDescriptorMap);
	}

	/**
	 * Use SimpleBeanContainer.addBean(T bean) instead of addItem
	 */
	@Override
	public Item addItem(Object itemId) throws UnsupportedOperationException {
		throw new UnsupportedOperationException("Use SimpleBeanContainer.addBean(T bean) instead of SimpleBeanContainer.addItem");
	}

	@Override
	public Object addItemAt(int index) throws UnsupportedOperationException {
		throw new UnsupportedOperationException("Use SimpleBeanContainer.addBean(T bean)");
	}

	@Override
	public Item addItemAt(int index, Object newItemId) throws UnsupportedOperationException {
		throw new UnsupportedOperationException("Use SimpleBeanContainer.addBean(T bean)");
	}

	@Override
	public Object addItemAfter(Object previousItemId) throws UnsupportedOperationException {
		throw new UnsupportedOperationException();
	}

	@Override
	public Collection<?> getContainerPropertyIds() {
		return propertyDescriptorMap.keySet();
	}

	@Override
	public Property getContainerProperty(Object itemId, Object propertyId) {
		return itemIdBeanItemMap.get(itemId).getItemProperty(propertyId);
	}

	@Override
	public Class<?> getType(Object propertyId) {
		return propertyDescriptorMap.get(propertyId).getPropertyType();
	}

	@Override
	public void sort(Object[] propertyId, boolean[] ascending) {
		sortContainer(propertyId, ascending);
	}

	@Override
	public Collection<?> getSortableContainerPropertyIds() {
		return getSortablePropertyIds();
	}

	@Override
	public void addContainerFilter(Filter filter) throws UnsupportedFilterException {
		addFilter(filter);
	}

	@Override
	public void removeContainerFilter(Filter filter) {
		removeFilter(filter);
	}

	@Override
	public void removeAllContainerFilters() {
		if (!getFilters().isEmpty()) {
			for (Item item : itemIdBeanItemMap.values()) {
				removeAllValueChangeListeners(item);
			}
			removeAllFilters();
		}
	}

	private void removeAllValueChangeListeners(Item item) {
		for (Object propertyId : item.getItemPropertyIds()) {
			removeValueChangeListener(item, propertyId);
		}
	}

	private void removeValueChangeListener(Item item, Object propertyId) {
		Property property = item.getItemProperty(propertyId);
		if (property instanceof ValueChangeNotifier) {
			((ValueChangeNotifier) property).removeListener(this);
		}
	}

	@Override
	public void valueChange(ValueChangeEvent event) {
		filterAll();
	}

	@Override
	public void addListener(ValueChangeListener listener) {
		addValueChangeListener(listener);
	}

	@Override
	public void removeListener(ValueChangeListener listener) {
		removeValueChangeListener(listener);
	}

	public boolean removeBean(T bean) {
		SimpleBeanItem<T> simpleBeanItem = getBeanItem(bean);

		Object itemId = simpleBeanItem.getItemId();

		int origSize = size();
		int position = indexOfId(itemId);

		boolean result = internalRemoveItem(itemId);
		if (result) {
			itemIdBeanItemMap.remove(itemId);
			entityMap.remove(getIdValue(bean));
			// detach listeners from Item
			removeAllValueChangeListeners(simpleBeanItem);
			// fire event only if the visible view changed, regardless of
			// whether filtered out items were removed or not
			if (size() != origSize) {
				fireItemRemoved(position, itemId);
			}
		}
		return result;
	}

	@Override
	public boolean removeItem(Object itemId) throws UnsupportedOperationException {
		throw new UnsupportedOperationException("Use removeBean(T bean)");
	}

	public SimpleBeanItem<T> getBeanItem(T entity) {
		return entityMap.get(getIdValue(entity));
	}

	protected T getBean(Object itemId) {
		SimpleBeanItem<T> item = getItem(itemId);
		if (item == null) {
			return null;
		}
		return item.getBean();
	}

	public Collection<T> getBeans() {
		LinkedHashSet<T> set = new LinkedHashSet<T>();
		Collection<SimpleBeanItem<T>> values = entityMap.values();
		for (SimpleBeanItem<T> simpleBeanItem : values) {
			set.add(simpleBeanItem.getBean());
		}
		return set;
	}

	public boolean removeAllItems() {
		int origSize = size();

		internalRemoveAllItems();

		// detach listeners from all Items
		for (Item item : entityMap.values()) {
			removeAllValueChangeListeners(item);
		}

		itemIdBeanItemMap.clear();
		entityMap.clear();
		// fire event only if the visible view changed, regardless of whether
		// filtered out items were removed or not
		if (origSize != 0) {
			fireItemSetChange();
		}

		return true;
	}

	@Override
	protected SimpleBeanItem<T> getUnfilteredItem(Object itemId) {
		return itemIdBeanItemMap.get(itemId);
	}

	public void addBeans(Collection<T> beans) {
		boolean modified = false;
		for (T bean : beans) {
			// TODO farklı tipte bir nesne ise eklemiyoruz
			if (bean == null || !clazz.isAssignableFrom(bean.getClass())) {
				continue;
			}

			SimpleBeanItem<T> simpleBeanItem = createBeanItem(bean);

			Object itemId = simpleBeanItem.getItemId();

			if (itemId == null) {
				throw new IllegalArgumentException("ItemId boş olamaz");
			}

			if (internalAddItemAtEnd(itemId, simpleBeanItem, false) != null) {
				modified = true;
			}
		}

		if (modified) {
			// Filter the contents when all items have been added
			if (isFiltered()) {
				filterAll();
			} else {
				fireItemSetChange();
			}
		}
	}

	@Override
	protected void registerNewItem(int position, Object itemId, SimpleBeanItem<T> item) {
		entityMap.put(getIdValue(item.getBean()), item);
		itemIdBeanItemMap.put(itemId, item);
	}

	public void setBeans(T... beans) {
		setBeans(Arrays.asList(beans));
	}

	public void setBeans(Collection<T> beans) {
		removeAllItems();
		addBeans(beans);
	}

	/**
	 * nesnenin id'si degismemeli
	 * 
	 * @param oldItem
	 * @param newItem
	 * @return Object itemdId
	 */
	public Object replaceItem(T oldItem, T newItem) {
		Object itemId = getBeanItem(oldItem).getItemId();
		int index = indexOfId(itemId);
		removeBean(oldItem);
		return addBeanAt(newItem, index).getItemId();
	}

	@Override
	public boolean addContainerProperty(Object propertyId, Class<?> type, Object defaultValue) throws UnsupportedOperationException {

		return false;
	}

	@Override
	protected boolean passesFilters(Object itemId) {
		SimpleBeanItem item = getUnfilteredItem(itemId);
		if (getFilters().isEmpty()) {
			return true;
		}
		final Iterator<Filter> i = getFilters().iterator();
		while (i.hasNext()) {
			final Filter f = i.next();
			if (f.passesFilter(itemId, item)) {
				return true;
			}
		}
		return false;
	}

	public int getItemCount() {
		return entityMap.size();
	}

	@Override
	public void addValueChangeListener(ValueChangeListener listener) {
		for (SimpleBeanItem<T> item : itemIdBeanItemMap.values()) {
			item.addListener(listener);
		}
	}

	@Override
	public void removeValueChangeListener(ValueChangeListener listener) {
		for (SimpleBeanItem<T> item : itemIdBeanItemMap.values()) {
			item.removeListener(listener);
		}
	}

	@Override
	public Collection<Filter> getContainerFilters() {
		return super.getContainerFilters();
	}
}
