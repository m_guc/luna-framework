package org.lunaframework.vaadin.component.grid;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.lunaframework.core.util.UtilsForReflection;
import org.lunaframework.vaadin.component.HtmlLabel;
import org.lunaframework.vaadin.component.NestedProperty;
import org.lunaframework.vaadin.component.combobox.AdvancedComboBox;
import org.lunaframework.vaadin.component.combobox.AdvancedComboBox.SelectionChangeListener;

import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.event.Action.Handler;
import com.vaadin.event.FieldEvents.TextChangeEvent;
import com.vaadin.event.FieldEvents.TextChangeListener;
import com.vaadin.event.dd.DropHandler;
import com.vaadin.server.Resource;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.AbstractTextField.TextChangeEventMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Table;
import com.vaadin.ui.Table.ColumnGenerator;
import com.vaadin.ui.Table.GeneratedRow;
import com.vaadin.ui.Table.RowGenerator;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.Runo;

public class DataGrid<T> extends VerticalLayout {

	private Table table;
	private String[] propertyNames;
	private String[] propertyTitles;
	private Class<T> clazz;
	private Map<String, AbstractGridCellRenderer> cellRendererMap = new HashMap<String, AbstractGridCellRenderer>();
	private TextField filterTextField;
	private boolean showFilter;
	private HorizontalLayout topToolBar;
	private String idFieldName;
	private Class[] propertyTypes;
	private List<ColumnCollapseListener> columnCollapseListeners = new ArrayList<ColumnCollapseListener>();
	private boolean invokeColumnCollapseListeners = true;
	private AdvancedComboBox<Map<String, String>> comboBoxFields;

	public DataGrid(Class<T> clazz, String[] propertyNames, String[] propertyTitles) {
		this(clazz, propertyNames, propertyTitles, false);
	}

	public DataGrid(Class<T> clazz, String[] propertyNames, String[] propertyTitles, boolean showFilter) {
		this(clazz, "id", null, propertyNames, propertyTitles, showFilter);
	}

	public DataGrid(Class<T> clazz, String idFieldName, String[] propertyNames, String[] propertyTitles, boolean showFilter) {
		this(clazz, idFieldName, null, propertyNames, propertyTitles, showFilter);
	}

	public DataGrid(Class<T> clazz, String idFieldName, Class[] propertyTypes, String[] propertyNames, String[] propertyTitles, boolean showFilter) {
		this.clazz = clazz;
		this.idFieldName = idFieldName;
		this.propertyNames = propertyNames;
		this.propertyTitles = propertyTitles;
		this.showFilter = showFilter;
		this.propertyTypes = propertyTypes;
		initComponents();
	}

	private void initComponents() {
		setShowFilter(showFilter);
		addComponent(getTopToolBar());
		addComponent(getTable());
		setExpandRatio(getTable(), 1.0f);
	}

	public void setShowFooter(boolean showFooter) {
		getTable().setFooterVisible(showFooter);
	}

	public void setShowFilter(boolean showFilter) {
		Iterator<Component> iterator = getTopToolBar().iterator();
		while (iterator.hasNext()) {
			Component component = (Component) iterator.next();
			if (getFilterTextField().equals(component)) {
				getTopToolBar().removeComponent(component);
				break;
			}
		}

		if (showFilter) {

			getTopToolBar().addComponent(getFieldComboBox());
			getTopToolBar().addComponent(getFilterTextField());

			// int componentCount = getTopToolBar().getComponentCount();
			// getTopToolBar().addComponent(getFilterTextField(), componentCount
			// > 1 ? 1 : 0);
			getTopToolBar().setExpandRatio(getFieldComboBox(), 1);
			getTopToolBar().setExpandRatio(getFilterTextField(), 3);
			getTopToolBar().setComponentAlignment(getFilterTextField(), Alignment.MIDDLE_LEFT);
		}
	}

	public HorizontalLayout getTopToolBar() {
		if (topToolBar == null) {
			topToolBar = new HorizontalLayout();
			topToolBar.setWidth("100%");
			topToolBar.setSpacing(true);
			topToolBar.setMargin(new MarginInfo(false, true, false, true));
		}
		return topToolBar;
	}

	private AdvancedComboBox<Map<String, String>> getFieldComboBox() {
		if (comboBoxFields == null) {
			comboBoxFields = new AdvancedComboBox<Map<String, String>>("isim", "baslik");
			comboBoxFields.setPlaceHolder("Tüm alanlarda ara");
			comboBoxFields.addSelectionChangeListener(new SelectionChangeListener<Map<String, String>>() {

				@Override
				public void onSelectionChange(Collection<Map<String, String>> items) {
					doFilter(getFilterTextField().getValue());
				}

			});

			Set<Map<String, String>> set = new LinkedHashSet<Map<String, String>>();
			// HashMap<String, String> itemTumu = new HashMap<String, String>();
			// itemTumu.put("isim", "Tümü");
			// itemTumu.put("baslik", "Tümü");
			// set.add(itemTumu);
			for (int i = 0; i < propertyNames.length; i++) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("isim", propertyNames[i]);
				map.put("baslik", propertyTitles[i]);
				set.add(map);
			}
			comboBoxFields.setItems(set);
		}
		return comboBoxFields;
	}

	public void setTitle(String title) {
		int componentCount = getTopToolBar().getComponentCount();
		if (componentCount > 0) {
			Component component = getTopToolBar().getComponent(0);
			if (component instanceof HtmlLabel) {
				getTopToolBar().removeComponent(component);
			}
		}
		HtmlLabel label = new HtmlLabel(title);
		label.setStyleName("h2");
		label.setSizeUndefined();
		getTopToolBar().addComponentAsFirst(label);
		getTopToolBar().setComponentAlignment(label, Alignment.MIDDLE_LEFT);
	}

	public void addColumnCollapseListener(ColumnCollapseListener listener) {
		this.columnCollapseListeners.add(listener);
	}

	public Table getTable() {
		if (table == null) {
			table = new Table() {

				@Override
				public void setColumnCollapsed(Object propertyId, boolean collapsed) throws IllegalStateException {
					super.setColumnCollapsed(propertyId, collapsed);
					if (!invokeColumnCollapseListeners) {
						return;
					}
					for (ColumnCollapseListener columnCollapseListener : DataGrid.this.columnCollapseListeners) {
						columnCollapseListener.columnCollapsed(getTable(), propertyId.toString(), collapsed);
					}
				}
			};
			table.setWidth("100%");
			table.setHeight("100%");
			table.setContainerDataSource(new SimpleBeanContainer<T>(idFieldName, clazz, propertyTypes, propertyNames));
			table.setVisibleColumns(propertyNames);
			table.setColumnHeaders(propertyTitles);
			table.setSelectable(true);
			table.addStyleName(Runo.TABLE_SMALL);
			table.setImmediate(true);
			table.setSortEnabled(true);
			table.setColumnReorderingAllowed(true);
			table.setColumnCollapsingAllowed(true);
			table.setRowGenerator(createRowGenerator());
		}
		return table;
	}

	private void setFilteredCount(int total, int filteredCount) {
		getTable().setColumnFooter(propertyNames[0], "<div style='white-space:nowrap;padding-top:2px'>" + filteredCount + "/" + total + "</div>");
	}

	private RowGenerator createRowGenerator() {
		return new RowGenerator() {
			@Override
			public GeneratedRow generateRow(Table table, Object itemId) {
				T bean = getContainer().getBean((Object) itemId);
				String[] values = new String[getTable().getVisibleColumns().length];
				int c = 0;
				for (Object columnName : getTable().getVisibleColumns()) {
					String propertyName = (String) columnName;
					Object value = UtilsForReflection.getValue(bean, propertyName);
					NestedProperty nestedProperty = (NestedProperty) getContainer().getContainerProperty(itemId, propertyName);

					AbstractGridCellRenderer gridCellRenderer = cellRendererMap.get(propertyName);

					values[c] = value == null ? "" : value.toString();

					if (gridCellRenderer != null) {
						values[c] = gridCellRenderer.getCellValue(bean, value, nestedProperty);
					} else if (value instanceof Boolean) {
						values[c] = Boolean.TRUE.equals(value) ? "Evet" : "Hayır";
					}
					c++;
				}
				GeneratedRow generateRow = new GeneratedRow(values);
				generateRow.setHtmlContentAllowed(true);
				return generateRow;
			}
		};
	}

	public void setColumnsCollapsed(boolean collapsed, String... propertyNames) {
		invokeColumnCollapseListeners = false;
		for (String string : propertyNames) {
			getTable().setColumnCollapsed(string, collapsed);
		}
		invokeColumnCollapseListeners = true;
	}

	public void setVisibleColumns(String... propertyNames) {
		getTable().setVisibleColumns(propertyNames);
	}

	public void setVisiblePropertyTitles(String... propertyTitles) {
		getTable().setColumnHeaders(propertyTitles);
	}

	public void setColumnVisibility(String propertyName, boolean visible) {
		Object[] visibleColumns = getTable().getVisibleColumns();
		List<Object> columns = new ArrayList<Object>(Arrays.asList(visibleColumns));

		if (!visible) {
			if (columns.contains(propertyName)) {
				columns.remove(propertyName);
			}
		} else {
			if (!columns.contains(propertyName)) {
				columns.add(propertyName);
			}
		}
		setVisibleColumns(columns.toArray(new String[0]));
	}

	public void addContainerPropertyId(Object propertyId, Class<?> type, Object defaultValue) {
		getTable().addContainerProperty(propertyId, type, defaultValue);
	}

	public void addGeneratedColumn(String id, ColumnGenerator generatedColumn) {
		getTable().addGeneratedColumn(id, generatedColumn);
	}

	public void setMultiSelect(boolean multiSelect) {
		getTable().setMultiSelect(multiSelect);
	}

	public void setItems(Collection<T> items) {
		getContainer().removeAllItems();
		getContainer().addBeans(items);
		doFilter("");
	}

	public SimpleBeanContainer<T> getContainer() {
		return (SimpleBeanContainer<T>) getTable().getContainerDataSource();
	}

	public SimpleBeanItem<T> getBeanItemByItemId(Object itemId) {
		return getContainer().getItem(itemId);
	}

	public T getBeanByItemId(Object itemId) {
		return getContainer().getBean(itemId);
	}

	public void selectNextItem() {
		T item = getSelectedBean();
		if (item == null) {
			return;
		}

		Object nextItemId = getContainer().nextItemId(getContainer().getBeanItem(item).getItemId());
		if (nextItemId == null) {
			nextItemId = getContainer().firstItemId();
		}

		if (getTable().isMultiSelect()) {
			getTable().setValue(null);
		}
		getTable().select(nextItemId);
		getTable().setCurrentPageFirstItemIndex(getContainer().indexOfId(nextItemId));
	}

	public void selectPrevItem() {

		T item = getSelectedBean();
		if (item == null) {
			return;
		}
		Object prevItemId = getContainer().prevItemId(getContainer().getBeanItem(item).getItemId());

		if (prevItemId == null) {
			prevItemId = getContainer().lastItemId();
		}

		if (getTable().isMultiSelect()) {
			getTable().setValue(null);
		}

		getTable().select(prevItemId);
		getTable().setCurrentPageFirstItemIndex(getContainer().indexOfId(prevItemId));
	}

	public List<T> getItems() {
		ArrayList<T> list = new ArrayList<T>();
		SimpleBeanContainer<T> beanItemContainer = (SimpleBeanContainer<T>) getTable().getContainerDataSource();
		Collection itemIds = beanItemContainer.getItemIds();
		for (Object object : itemIds) {
			list.add((T) beanItemContainer.getItem(object).getBean());
		}
		return list;
	}

	public void addItemAt(T item, int index) {
		getContainer().addBeanAt(item, index);
	}

	public void addItem(T item) {
		getContainer().addBean(item);
	}

	public void removeItems(T... items) {
		for (T t : items) {
			getContainer().removeBean(t);
		}
	}

	public void removeItems(Collection<T> items) {
		for (T t : items) {
			getContainer().removeBean(t);
		}
	}

	public void removeAllItems() {
		getContainer().removeAllItems();
	}

	public String getSelectedItemId() {
		return (String) getTable().getValue();
	}

	public T getSelectedBean() {
		if (getTable().isMultiSelect()) {
			if (getSelectedBeans() == null || getSelectedBeans().size() == 0) {
				return null;
			}
			return getSelectedBeans().iterator().next();
		}

		Object value = (Object) getTable().getValue();
		if (value == null) {
			return null;
		}
		return getContainer().getBean(value);
	}

	public Set<T> getSelectedBeans() {
		LinkedHashSet<T> selectedBeans = new LinkedHashSet<T>();
		if (getTable().isMultiSelect()) {
			Set<Object> value = (Set<Object>) getTable().getValue();

			for (Object id : value) {
				T bean = getContainer().getBean(id);
				if (bean == null) {
					continue;
				}

				selectedBeans.add(bean);
			}
		} else {
			T bean = getContainer().getBean((Object) getTable().getValue());
			if (bean != null) {
				selectedBeans.add(bean);
			}
		}
		return selectedBeans;
	}

	public void addValueChangeListener(ValueChangeListener valueChangelistener) {
		getTable().addValueChangeListener(valueChangelistener);
	}

	public void replaceItem(T oldItem, T newItem, boolean select) {
		Object itemId = getContainer().replaceItem(oldItem, newItem);

		if (select) {
			getTable().setValue(itemId);
		}
	}

	public void setSelectedItem(T item) {
		SimpleBeanItem<T> beanItem = getContainer().getBeanItem(item);
		if (beanItem == null) {
			getTable().setValue(null);
			return;
		}
		getTable().setValue(beanItem.getItemId());
	}

	public void addActionHandler(Handler handler) {
		getTable().addActionHandler(handler);
	}

	public void setCellRenderer(String propertyName, AbstractGridCellRenderer cellRenderer) {
		cellRendererMap.put(propertyName, cellRenderer);
	}

	public void setColumnExpandRatio(String propertyName, float expandRatio) {
		getTable().setColumnExpandRatio(propertyName, expandRatio);
	}

	public void setUseRowGenerator(boolean useRowGenerator) {
		if (useRowGenerator) {
			getTable().setRowGenerator(createRowGenerator());
		} else {
			getTable().setRowGenerator(null);
		}
	}

	public void setDropHandler(DropHandler dropHandler) {
		getTable().setDropHandler(dropHandler);
	}

	public void sort(Map<String, Boolean> columns) {
		Set<String> keySet = columns.keySet();
		ArrayList<String> names = new ArrayList<String>(columns.size());
		boolean[] orders = new boolean[columns.size()];
		int c = 0;
		for (String columnName : keySet) {
			names.add(columnName);
			orders[c++] = columns.get(columnName);
		}
		getTable().sort(names.toArray(), orders);
	}

	public void sort(Object[] propertyIds, boolean[] ascending) {
		getTable().sort(propertyIds, ascending);
	}

	public TextField getFilterTextField() {
		if (filterTextField == null) {
			filterTextField = new TextField();
			filterTextField.setWidth("100%");
			filterTextField.setInputPrompt("Arama ifadesi girin");
			filterTextField.setTextChangeEventMode(TextChangeEventMode.LAZY);
			filterTextField.setTextChangeTimeout(200);
			filterTextField.addTextChangeListener(new TextChangeListener() {
				@Override
				public void textChange(TextChangeEvent event) {
					doFilter(event.getText().trim());
				}
			});
		}
		return filterTextField;
	}

	public void doFilter(String text) {
		getContainer().removeAllContainerFilters();

		String[] propertyNames = null;

		Collection<Map<String, String>> selectedItems = getFieldComboBox().getSelectedItems();
		if (selectedItems != null && selectedItems.size() > 0) {
			Collection<String> collectProperty = (Collection<String>) UtilsForReflection.collectProperty(selectedItems, "isim");
			propertyNames = collectProperty.toArray(new String[0]);
		} else {
			propertyNames = this.propertyNames;
		}

		for (String propertyName : propertyNames) {
			if (org.apache.commons.lang.StringUtils.isNotBlank(text)) {
				StringContainsFilter containsFilter = new StringContainsFilter(propertyName, text) {
					@Override
					protected String getPropertyStringValue(Object bean, Object propertyValue, NestedProperty nestedProperty) {
						AbstractGridCellRenderer cellRenderer = cellRendererMap.get(nestedProperty.getPropertyName());
						if (cellRenderer == null) {
							return super.getPropertyStringValue(bean, propertyValue, nestedProperty);
						} else {
							return cellRenderer.getCellValueAsString(bean, propertyValue, nestedProperty);
						}
					}
				};
				getContainer().addContainerFilter(containsFilter);
			}
		}
		Collection<?> itemIds = getContainer().getItemIds();
		int filteredCount = itemIds.size();
		int total = getContainer().getItemCount();
		setFilteredCount(total, filteredCount);
	}

	public int indexOf(T entity) {
		return getContainer().indexOfId(getContainer().getBeanItem(entity).getItemId());
	}

	public void refreshRows() {
		getTable().refreshRowCache();
	}

	public void addToolBarComponent(Component component) {
		addToolBarComponent(component, Alignment.MIDDLE_RIGHT);
	}

	public void addToolBarComponent(Component component, Alignment alignment) {
		getTopToolBar().addComponent(component);
		getTopToolBar().setComponentAlignment(component, alignment);
	}

	public void setColumnIcon(String columnName, Resource icon) {
		getTable().setColumnIcon(columnName, icon);
	}

	public String[] getVisibleColumns() {
		ArrayList<String> list = new ArrayList<String>();
		Object[] visibleColumns = getTable().getVisibleColumns();
		for (Object object : visibleColumns) {
			if (!getTable().isColumnCollapsed(object)) {
				list.add(object.toString());
			}
		}
		return list.toArray(new String[0]);
	}

}