package org.lunaframework.vaadin.component.grid;

import com.vaadin.ui.Table;

public interface ColumnCollapseListener {

	public void columnCollapsed(Table table,String columnName,boolean collapsed);
}
