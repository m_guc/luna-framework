package org.lunaframework.vaadin.component;

import org.lunaframework.core.model.DateRange;

import com.vaadin.data.Validator.InvalidValueException;
import com.vaadin.ui.Component;
import com.vaadin.ui.CustomField;
import com.vaadin.ui.DateField;
import com.vaadin.ui.HorizontalLayout;

public class DateRangeField extends CustomField<DateRange> {

	private DateField lowerBoundDateField = new DateField();
	private DateField upperBoundDateField = new DateField();
//	private String dateFormat;

	public DateRangeField() {
		setDateFormat("dd/MM/yyyy");
	}
	
	@Override
	public Class<? extends DateRange> getType() {
		return DateRange.class;
	}

	@Override
	public void setBuffered(boolean buffered) {
		super.setBuffered(buffered);
		lowerBoundDateField.setBuffered(buffered);
		upperBoundDateField.setBuffered(buffered);
	}

	@Override
	public void commit() throws SourceException, InvalidValueException {
		super.commit();
		lowerBoundDateField.commit();
		upperBoundDateField.commit();
	}

	@Override
	protected DateRange getInternalValue() {
		return new DateRange(lowerBoundDateField.getValue(), upperBoundDateField.getValue());
	}

	@Override
	protected void setInternalValue(DateRange newValue) {
		super.setInternalValue(newValue);
		if(newValue==null) {
			lowerBoundDateField.setValue(null);
			upperBoundDateField.setValue(null);
			return;
		}
		lowerBoundDateField.setValue(newValue.getLowerBound());
		upperBoundDateField.setValue(newValue.getUpperBound());
	}

	@Override
	protected Component initContent() {
//		lowerBoundDateField.setDateFormat("dd/MM/yyyy");
//		upperBoundDateField.setDateFormat("dd/MM/yyyy");
		
		lowerBoundDateField.setWidth("80%");
		upperBoundDateField.setWidth("80%");
		
		HorizontalLayout horizontalLayout = new HorizontalLayout();
		horizontalLayout.addComponent(lowerBoundDateField);
		horizontalLayout.addComponent(upperBoundDateField);
		
//		horizontalLayout.setExpandRatio(lowerBoundDateField, 1);
//		horizontalLayout.setExpandRatio(upperBoundDateField, 1);
		horizontalLayout.setWidth("100%");
		
		lowerBoundDateField.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(com.vaadin.data.Property.ValueChangeEvent event) {
				DateRangeField.this.setValue(new DateRange(lowerBoundDateField.getValue(),upperBoundDateField.getValue()));
			}
		});
		upperBoundDateField.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(com.vaadin.data.Property.ValueChangeEvent event) {
				DateRangeField.this.setValue(new DateRange(lowerBoundDateField.getValue(),upperBoundDateField.getValue()));
			}
		});
		return horizontalLayout;
	}
	
	public void setLowerBoundDateFieldWidth(String width){
		lowerBoundDateField.setWidth(width);
	}
	
	public void setUpperBoundDateFieldWidth(String width){
		upperBoundDateField.setWidth(width);
	}
	
	public void setDateFormat(String dateFormat){
//		this.dateFormat = dateFormat;
		lowerBoundDateField.setDateFormat(dateFormat);
		upperBoundDateField.setDateFormat(dateFormat);
	}
}
