package org.lunaframework.vaadin.component;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import com.vaadin.server.StreamResource.StreamSource;

public class ByteArraySource implements StreamSource {
	private final byte[] byteArray2;

	public ByteArraySource(byte[] byteArray) {
		byteArray2 = byteArray;
	}

	@Override
	public InputStream getStream() {
		return new ByteArrayInputStream(byteArray2);
	}
}