package org.lunaframework.vaadin.component;

import com.vaadin.ui.Component;
import com.vaadin.ui.VerticalLayout;

public class TitledPanel extends VerticalLayout {

	public TitledPanel(String title, Component contentComponent) {
		setSizeFull();

		StringBuilder builder = new StringBuilder();
		builder.append("<div style='font-size:16px;padding-left:3px;padding-bottom:2px;margin-bottom:3px;border-bottom:1px solid #bbb'>");
		builder.append(title);
		builder.append("</div>");

		addComponent(new HtmlLabel(builder.toString()));
		addComponent(contentComponent);

		setExpandRatio(contentComponent, 1.0f);
	}
}
