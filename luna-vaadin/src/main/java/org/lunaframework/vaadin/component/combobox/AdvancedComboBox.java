package org.lunaframework.vaadin.component.combobox;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.lunaframework.core.util.UtilsForReflection;

import com.vaadin.annotations.JavaScript;
import com.vaadin.annotations.StyleSheet;
import com.vaadin.ui.AbstractJavaScriptComponent;
import com.vaadin.ui.JavaScriptFunction;

@JavaScript({ "AdvancedComboBox.js", "jquery.min.js", "select2.min.js", "select2_locale_tr.js" })
@StyleSheet({ "select2.css", "select2-spinner.gif", "select2.png" })
public class AdvancedComboBox<T> extends AbstractJavaScriptComponent {

	private String idField;
	private String displayField;
	private Map<String, T> idToItemMap = new HashMap<String, T>();

	private List<SelectionChangeListener<T>> selectionChangeListeners = new ArrayList<AdvancedComboBox.SelectionChangeListener<T>>(0);

	private Collection<T> items;
	private Collection<T> selectedItems = new ArrayList<>();

	public AdvancedComboBox() {
		this("id", "text");
	}

	public AdvancedComboBox(String idField, String displayField) {
		this.setIdField(idField);
		this.setDisplayField(displayField);
		setWidth("100%");
		addCallbackFunctions();
	}

	private void addCallbackFunctions() {
		addFunction("selectItems", new JavaScriptFunction() {
			public void call(JSONArray arguments) throws JSONException {
				selectItems(arguments.getJSONArray(0));
			}
		});
	}

	public void setItems(Collection<T> items) {
		this.items = items;
		idToItemMap = new HashMap<String, T>();

		List<Map<String, Object>> rowItems = new ArrayList<Map<String, Object>>(items.size());
		for (T item : this.items) {
			HashMap<String, Object> map = new HashMap<String, Object>();
			String idValue = getIdValue(item);
			idToItemMap.put(idValue, item);
			map.put("id", idValue);
			map.put("text", getDisplayValue(item));
			rowItems.add(map);
		}

		getState().items = rowItems;
		markAsDirty();
	}

	public Collection<T> getSelectedItems() {
		return this.selectedItems;
	}

	public void setSelectedItems(T... items) {
		setSelectedItems(Arrays.asList(items));
	}

	public void setSelectedItems(Collection<T> items) {
		this.selectedItems = new ArrayList<T>();

		if (items != null) {
			this.selectedItems.addAll(items);
		}

		List<String> ids = new ArrayList<String>(this.selectedItems.size());
		for (T t : this.selectedItems) {
			ids.add(getIdValue(t));
		}
		getState().selectedItems = ids;
		markAsDirty();
	}

	protected void selectItems(JSONArray jsonArray) {
		this.selectedItems = new ArrayList<T>();
		getState().selectedItems = new ArrayList<String>();

		for (int c = 0; c < jsonArray.length(); c++) {
			try {
				String object = (String) jsonArray.get(c);
				if (object != null) {
					object = object.trim();
				}
				this.selectedItems.add(idToItemMap.get(object));
				getState().selectedItems.add(object);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		fireSelectionChangeListeners();
	}

	private void fireSelectionChangeListeners() {
		for (SelectionChangeListener<T> selectionChangeListener : selectionChangeListeners) {
			selectionChangeListener.onSelectionChange(this.selectedItems);
		}
	}

	private String getIdValue(T item) {
		return getValueAsString(item, getIdField());
	}

	protected String getDisplayValue(T item) {
		return getValueAsString(item, getDisplayField());
	}

	protected String getValueAsString(T item, String fieldName) {
		if (item == null) {
			return null;
		}

		if (fieldName == null) {
			return item.toString();
		}

		Object value = UtilsForReflection.getValue(item, fieldName);
		if (value == null) {
			return null;
		}

		return value.toString().trim();
	}

	public void addSelectionChangeListener(SelectionChangeListener listener) {
		this.selectionChangeListeners.add(listener);
	}

	@Override
	protected DefaultComboBoxState getState(boolean markAsDirty) {
		return (DefaultComboBoxState) super.getState(markAsDirty);
	}

	@Override
	protected DefaultComboBoxState<T> getState() {
		return (DefaultComboBoxState<T>) super.getState();
	}

	public String getIdField() {
		return idField;
	}

	public void setIdField(String idField) {
		this.idField = idField;
	}

	public String getDisplayField() {
		return displayField;
	}

	public void setDisplayField(String displayField) {
		this.displayField = displayField;
	}

	public void setPlaceHolder(String placeHolder) {
		getState().placeHolder = placeHolder;
		markAsDirty();
	}

	public String getPlaceHolder() {
		return getState().placeHolder;
	}

	public interface SelectionChangeListener<T> {

		public void onSelectionChange(Collection<T> items);
	}

}
