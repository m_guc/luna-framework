package org.lunaframework.vaadin.component;

import java.util.Arrays;
import java.util.Collection;

import org.lunaframework.core.model.DateRange;
import org.lunaframework.vaadin.DefaultStringToNumberConverter;
import org.lunaframework.vaadin.component.combobox.AdvancedComboBoxField;

import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.ui.AbstractTextField;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;

public class AdvancedFormFieldFactory extends DefaultFieldFactory {

	@Override
	public Field createField(Item item, Object propertyId, Component uiContext) {
		Class<?> type = item.getItemProperty(propertyId).getType();

		Field field = null;
		if (Enum.class.isAssignableFrom(type)) {
			Enum[] enumConstants = (Enum[]) type.getEnumConstants();
			ComboBox comboBox = new ComboBox();
			comboBox.setContainerDataSource(new BeanItemContainer(type, Arrays.asList(enumConstants)));
			field = comboBox;
		} else if (DateRange.class.isAssignableFrom(type)) {
			field = new DateRangeField();
		} else if (Boolean.class.isAssignableFrom(type)) {
			ComboBox comboBox = new ComboBox();
			comboBox.addItem(Boolean.TRUE);
			comboBox.addItem(Boolean.FALSE);
			comboBox.setItemCaption(Boolean.TRUE, "Evet");
			comboBox.setItemCaption(Boolean.FALSE, "Hayır");
			comboBox.setNullSelectionAllowed(false);
			field = comboBox;
		} else if (Collection.class.isAssignableFrom(type)) {
			AdvancedComboBoxField comboBox = new AdvancedComboBoxField();
			field = comboBox;
		} else {
			field = createFieldByPropertyType(type);
		}
		field.setCaption(createCaptionByPropertyId(propertyId));

		if (field instanceof AbstractTextField) {
			AbstractTextField abstractTextField = (AbstractTextField) field;
			abstractTextField.setNullRepresentation("");

			if (Number.class.isAssignableFrom(type)) {
				abstractTextField.setConverter(new DefaultStringToNumberConverter());
			}
		}

		return field;
	}
}
