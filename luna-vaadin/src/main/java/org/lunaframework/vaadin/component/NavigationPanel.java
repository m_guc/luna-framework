package org.lunaframework.vaadin.component;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.lunaframework.vaadin.view.INavigatorUI;
import org.lunaframework.vaadin.view.ViewMetaData;

import com.vaadin.server.ExternalResource;
import com.vaadin.server.Page;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.NativeButton;
import com.vaadin.ui.VerticalLayout;

public class NavigationPanel extends VerticalLayout implements ClickListener, INavigatorUI {

	protected Map<String, NativeButton> buttons = new HashMap<String, NativeButton>();
	private CssLayout menu;

	public NavigationPanel() {
		initComponents();
	}

	private void initComponents() {
		addStyleName("sidebar");
		setWidth(null);
		setHeight("100%");

		addComponent(createBrandingPanel());
		addComponent(getMenu());
		addComponent(createProfileMenu());

		setExpandRatio(getMenu(), 1);
	}

	protected Component createProfileMenu() {
		return new VerticalLayout();
	}

	private CssLayout getMenu() {
		if (menu == null) {
			menu = new CssLayout();
			menu.addStyleName("menu");
			menu.setHeight("100%");
		}
		return menu;
	}

	protected Component createBrandingPanel() {
		Label logo = new Label(getBrandingText(), ContentMode.HTML);
		logo.setSizeUndefined();
		CssLayout layout = new CssLayout();
		layout.addStyleName("branding");
		layout.addComponent(logo);
		return layout;
	}

	protected String getBrandingText() {
		return "<span>Branding</span> Text";
	}

	protected void setButtonSelected(Button button) {
		Iterator<Component> iterator = getMenu().iterator();
		while (iterator.hasNext()) {
			Component component = (Component) iterator.next();
			if (component instanceof NativeButton) {
				if (!button.equals(component)) {
					component.removeStyleName("selected");
				}
			}
		}
		button.addStyleName("selected");
	}

	@Override
	public void addView(ViewMetaData viewMetaData) {
		NativeButton viewButton = new NativeButton(viewMetaData.getTitle());
		viewButton.addStyleName(viewMetaData.getStyleName());

		String iconPath = viewMetaData.getIconPath();
		if (iconPath != null) {
			viewButton.setIcon(new ExternalResource(iconPath));
		}

		viewButton.addClickListener(this);
		viewButton.setData(viewMetaData);

		getMenu().addComponent(viewButton);
		buttons.put(viewMetaData.getName(), viewButton);
	}

	@Override
	public void buttonClick(ClickEvent event) {
		Page.getCurrent().setUriFragment("!" + ((ViewMetaData) event.getButton().getData()).getName());
	}

	@Override
	public void viewSelected(ViewMetaData viewMetaData) {
		if (viewMetaData == null) {
			return;
		}

		Iterator<Component> iterator = getMenu().iterator();
		while (iterator.hasNext()) {
			NativeButton button = (NativeButton) iterator.next();
			button.removeStyleName("selected");
			if (viewMetaData.equals(button.getData())) {
				button.addStyleName("selected");
			}
		}
	}
}
