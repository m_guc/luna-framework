package org.lunaframework.vaadin.component;

import org.lunaframework.vaadin.component.grid.DataGrid;

import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;

public class BaseGridPanel<T> extends VerticalLayout {

	private HorizontalLayout topBar;
	private DataGrid<T> grid;
	private ButtonPanel buttonPanel;
	private Class<T> type;
	private final String[] propertyNames;
	private final String[] propertyTitles;
	private final boolean buttonBarAtBottom;

	public BaseGridPanel(Class<T> type, String[] propertyNames, String[] propertyTitles) {
		this(type, propertyNames, propertyTitles, false);
	}

	public BaseGridPanel(Class<T> type, String[] propertyNames, String[] propertyTitles, boolean buttonBarAtBottom) {
		this.type = type;
		this.propertyNames = propertyNames;
		this.propertyTitles = propertyTitles;
		this.buttonBarAtBottom = buttonBarAtBottom;
		initComponents();
	}

	protected void initComponents() {
		setSizeFull();

		addComponent(getGrid());
		setExpandRatio(getGrid(), 1.0f);

		if (hasButtonBar()) {
			if (isButtonBarAtBottom()) {
				addComponent(getTopBar());
			} else {
				addComponentAsFirst(getTopBar());
			}
			addActions();
		}
	}

	protected boolean hasButtonBar() {
		return true;
	}

	private void addActions() {
		// TODO Auto-generated method stub

	}

	protected HorizontalLayout getTopBar() {
		if (topBar == null) {
			topBar = new HorizontalLayout();
			topBar.setMargin(true);
			topBar.setSpacing(true);
			topBar.setWidth("100%");
		}
		return topBar;
	}

	protected DataGrid<T> getGrid() {
		if (grid == null) {
			grid = new DataGrid<T>(type, propertyNames, propertyTitles, showFilter());
			grid.setSizeFull();
		}
		return grid;

	}

	protected boolean showFilter() {
		return false;
	}

	protected ButtonPanel getButtonPanel() {
		if (buttonPanel == null) {
			buttonPanel = new ButtonPanel();
		}
		return buttonPanel;
	}

	public boolean isButtonBarAtBottom() {
		return buttonBarAtBottom;
	}

}
