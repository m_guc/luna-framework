package org.lunaframework.vaadin.component.combobox;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.vaadin.shared.ui.JavaScriptComponentState;

public class AutoCompleteFieldState extends JavaScriptComponentState {

	public List<Map<String, Object>> items = new ArrayList<Map<String, Object>>();
	public List<Map<String, Object>> selectedItems = new ArrayList<Map<String, Object>>();
	
	public String queryString;
	public String placeHolder;
}
