package org.lunaframework.vaadin.component.combobox;

import java.util.Collection;
import java.util.Map;

import com.vaadin.shared.ui.JavaScriptComponentState;

public class DefaultComboBoxState<T> extends JavaScriptComponentState {

	public Collection<String> selectedItems;
	public Collection<Map<String, Object>> items;
	public String placeHolder;
}
