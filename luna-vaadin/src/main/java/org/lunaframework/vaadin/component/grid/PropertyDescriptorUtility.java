package org.lunaframework.vaadin.component.grid;

import java.util.LinkedHashMap;
import java.util.Map;

import org.lunaframework.core.util.UtilsForReflection;
import org.lunaframework.vaadin.component.PropertyDescriptor;

public class PropertyDescriptorUtility {

	public static Map<String, PropertyDescriptor> createPropertyDescriptorsByClazz(Class<?> clazz, String[] propertyNames) {
		Map<String, PropertyDescriptor> propertyMap = new LinkedHashMap<String, PropertyDescriptor>();
		Class[] propertyTypes = new Class[propertyNames.length];

		int c = 0;
		for (String propertyName : propertyNames) {
			Class propertyType;
			if (propertyName.contains("[")) {
				propertyType = String.class;
			} else {
				propertyType = UtilsForReflection.getNestedGetterMethod(clazz, propertyName).getReturnType();
			}
			propertyTypes[c] = propertyType;
			c++;
		}
		return createPropertyDescriptors(propertyTypes, propertyNames);
	}

	public static Map<String, PropertyDescriptor> createPropertyDescriptors(Class[] propertyTypes, String[] propertyNames) {
		Map<String, PropertyDescriptor> propertyMap = new LinkedHashMap<String, PropertyDescriptor>();
		int c = 0;
		for (String propertyName : propertyNames) {
			Class propertyType = propertyTypes[c];
			if (propertyName.contains("[")) {
				propertyType = String.class;
			}
			propertyMap.put(propertyName, new PropertyDescriptor(propertyType, propertyName));
			c++;
		}
		return propertyMap;
	}
}
