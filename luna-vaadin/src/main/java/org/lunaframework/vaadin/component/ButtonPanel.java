package org.lunaframework.vaadin.component;

import java.util.Iterator;

import com.vaadin.ui.AbstractComponent;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;

public class ButtonPanel extends HorizontalLayout {

	public ButtonPanel() {
		setSpacing(true);
	}

	public void addButtons(Button... buttons) {
		for (Button button : buttons) {
//			button.addStyleName(Runo.BUTTON_DEFAULT);
			addComponent(button);
		}
	}

	public void clearErrors() {
		Iterator<Component> componentIterator = getComponentIterator();
		while (componentIterator.hasNext()) {
			Component component = (Component) componentIterator.next();
			if (component instanceof AbstractComponent) {
				((AbstractComponent) component).setComponentError(null);
			}

		}
	}
}
