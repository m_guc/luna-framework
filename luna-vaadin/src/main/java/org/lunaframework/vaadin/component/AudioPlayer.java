package org.lunaframework.vaadin.component;

import com.vaadin.server.Page;

public class AudioPlayer {

	private String src;

	public String getSrc() {
		return src;
	}

	public void setSrc(String src) {
		this.src = src;
	}

	public void play() {
		Page.getCurrent().getJavaScript().execute("(function(){var audio = new Audio();audio.src='" + src + "';audio.play()})()");
	}
}
