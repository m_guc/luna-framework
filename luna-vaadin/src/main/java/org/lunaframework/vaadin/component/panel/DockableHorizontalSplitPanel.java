package org.lunaframework.vaadin.component.panel;

import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalSplitPanel;

/**
 * A HorizontalSplitPanel implementation that has ability to show and hide
 * components
 * 
 * @author yawo
 * 
 */
public class DockableHorizontalSplitPanel extends HorizontalSplitPanel {

	public DockableHorizontalSplitPanel() {
		// TODO Auto-generated constructor stub
	}

	public void hideFirst() {
		hideComponent(getFirstComponent());
	}

	public void hideSecond() {
		hideComponent(getSecondComponent());
	}

	public void showFirst(float size) {
		showComponent(getFirstComponent(), size);
	}

	public void showSecond(float size) {
		showComponent(getSecondComponent(), size);
	}

	public void hideComponent(Component component) {

		Component firstComponent = getFirstComponent();
		if (firstComponent.equals(component)) {
			setSplitPosition(0, UNITS_PIXELS);
			setMaxSplitPosition(0, UNITS_PIXELS);
		}

		Component secondComponent = getSecondComponent();
		if (secondComponent.equals(component)) {
			setSplitPosition(0, UNITS_PIXELS, true);
			setMaxSplitPosition(0, UNITS_PIXELS);
		}
	}

	public void showComponent(Component component, float size) {

		Component firstComponent = getFirstComponent();
		if (firstComponent.equals(component)) {
			if (size > 1) {
				setMaxSplitPosition(size, UNITS_PIXELS);
				setSplitPosition(size, UNITS_PIXELS);
			} else {
				setMaxSplitPosition(size * 100, UNITS_PERCENTAGE);
				setSplitPosition(size * 100, UNITS_PERCENTAGE);
			}
		}

		Component secondComponent = getSecondComponent();
		if (secondComponent.equals(component)) {
			if (size > 1) {
				setMaxSplitPosition(size, UNITS_PIXELS);
				setSplitPosition(size, UNITS_PIXELS, true);
			} else {
				setMaxSplitPosition(size * 100, UNITS_PERCENTAGE);
				setSplitPosition(size * 100, UNITS_PERCENTAGE, true);
			}
		}
	}
}
