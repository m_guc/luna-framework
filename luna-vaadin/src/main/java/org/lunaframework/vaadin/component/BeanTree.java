package org.lunaframework.vaadin.component;

import java.util.Collection;

import org.lunaframework.core.util.UtilsForReflection;

import com.vaadin.event.DataBoundTransferable;
import com.vaadin.event.Transferable;
import com.vaadin.event.dd.DragAndDropEvent;
import com.vaadin.event.dd.DropHandler;
import com.vaadin.event.dd.acceptcriteria.AcceptCriterion;
import com.vaadin.event.dd.acceptcriteria.SourceIsTarget;
import com.vaadin.ui.Tree;

public class BeanTree<T> extends Tree {

	private String displayFieldName;
	private String hierarchyFieldName;

	public BeanTree(String displayFieldName, String hierarchyFieldName) {
		this.displayFieldName = displayFieldName;
		this.hierarchyFieldName = hierarchyFieldName;
	}

	public void addDragDropListener(final TreeDragDropListener listener) {
		setDragMode(TreeDragMode.NODE);
		setDropHandler(new DropHandler() {
			@Override
			public AcceptCriterion getAcceptCriterion() {
				return SourceIsTarget.get();
			}

			@Override
			public void drop(DragAndDropEvent event) {
				Transferable t = event.getTransferable();

				if (!(t instanceof DataBoundTransferable)) {
					return;
				}

				DataBoundTransferable transferable = (DataBoundTransferable) t;

				TreeTargetDetails targetData = (TreeTargetDetails) event.getTargetDetails();

				Object sourceItemId = transferable.getItemId();
				Object targetItemId = targetData.getItemIdOver();
				listener.onDragDrop(sourceItemId, targetItemId);
			}
		});
	}

	@Override
	public String getItemCaption(Object itemId) {
		return UtilsForReflection.getValueByExpression(itemId, displayFieldName);
	}

	public void setBeans(Collection<T> beans) {
		for (T bean : beans) {
			T parent = (T) UtilsForReflection.getValue(bean, hierarchyFieldName);

			if (beans.contains(parent) && !containsId(parent)) {
				addItem(parent);
			}
			addBean(bean);
		}

		for (T t : beans) {
			setChildrenAllowed(t, hasChildren(t));
		}

	}

	public void addBean(T bean) {
		addItem(bean);
		setChildrenAllowed(bean, false);
		T parent = (T) UtilsForReflection.getValue(bean, hierarchyFieldName);
		if (parent != null && containsId(parent)) {
			setChildrenAllowed(parent, true);
			setParent(bean, parent);
			expandItem(parent);
		}
	}

	public void removeBean(T bean) {
		Object parent = getParent(bean);
		removeItem(bean);
		if (parent != null) {
			setChildrenAllowed(parent, hasChildren(parent));
		}
	}

	public T getSelectedBean() {
		return (T) getValue();
	}

	public void setSelectedBean(T bean) {
		setValue(bean);
	}

	public void addValueChangeListener(ValueChangeListener valueChangeListener) {
		// addListener(valueChangeListener); düzenlenmesi gerek
		super.addValueChangeListener(valueChangeListener);
	}

	public interface TreeDragDropListener {

		public void onDragDrop(Object sourceItemId, Object targetItemId);

	}

}
