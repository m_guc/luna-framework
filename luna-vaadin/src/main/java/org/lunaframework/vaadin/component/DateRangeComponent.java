package org.lunaframework.vaadin.component;

import java.util.Collection;
import java.util.Date;

import org.apache.commons.lang.time.DateUtils;
import org.lunaframework.core.model.DateRange;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.DateField;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.NativeButton;

public class DateRangeComponent extends HorizontalLayout implements ClickListener {

	private DateField fieldLowerBound;
	private DateField fieldUpperBound;
	private Button btnPrevious;
	private Button btnNext;
	private final boolean showMoveButtons;

	public DateRangeComponent() {
		this(new DateRange(DateUtils.addDays(new Date(), -7), new Date()));
	}

	public DateRangeComponent(DateRange dateRange) {
		this(dateRange, false);
	}

	public DateRangeComponent(DateRange dateRange, boolean showMoveButtons) {
		this.showMoveButtons = showMoveButtons;
		this.setDateRange(dateRange);
		initComponents();
	}

	private void initComponents() {
		// setSpacing(true);
		if (showMoveButtons) {
			btnPrevious = new NativeButton("<", this);
			btnNext = new NativeButton(">", this);
		}

		if (showMoveButtons) {
			addComponent(btnPrevious);
		}

		addComponent(getFieldLowerBound());
		addComponent(getFieldUpperBound());

		if (showMoveButtons) {
			addComponent(btnNext);
		}

		setExpandRatio(getFieldLowerBound(), 1.0f);
		setExpandRatio(getFieldUpperBound(), 1.0f);

		setDateFormat("dd.MM.yy");
	}

	private DateField getFieldLowerBound() {
		if (fieldLowerBound == null) {
			fieldLowerBound = new DateField();
			fieldLowerBound.setWidth("100%");
		}
		return fieldLowerBound;
	}

	private DateField getFieldUpperBound() {
		if (fieldUpperBound == null) {
			fieldUpperBound = new DateField();
			fieldUpperBound.setWidth("100%");
		}
		return fieldUpperBound;
	}

	public DateRange getDateRange() {
		return new DateRange((Date) getFieldLowerBound().getValue(), (Date) getFieldUpperBound().getValue());
	}

	public void setDateRange(DateRange dateRange) {
		getFieldLowerBound().setValue(dateRange.getLowerBound());
		getFieldUpperBound().setValue(dateRange.getUpperBound());
	}

	public void setDateFormat(String format) {
		setDateFormat(format, format);
	}

	public void setDateFormat(String lowerBoundFormat, String upperBoundFormat) {
		getFieldLowerBound().setDateFormat(lowerBoundFormat);
		getFieldUpperBound().setDateFormat(upperBoundFormat);
	}

	public void addListener(ValueChangeListener valueChangeListener) {
		getFieldLowerBound().addListener(valueChangeListener);
		getFieldUpperBound().addListener(valueChangeListener);
	}

	public void setImmediate(boolean immediate) {
		getFieldLowerBound().setImmediate(immediate);
		getFieldUpperBound().setImmediate(immediate);
	}

	public void previous() {
		moveDate(false);
	}

	public void next() {
		moveDate(true);
	}

	protected void moveDate(boolean next) {
		Date lowerBound = (Date) getFieldLowerBound().getValue();
		Date upperBound = (Date) getFieldUpperBound().getValue();

		long diff = upperBound.getTime() - lowerBound.getTime();

		Date lowerBound2;
		Date upperBound2;

		if (next) {
			lowerBound2 = new Date(lowerBound.getTime() + diff);
			upperBound2 = new Date(upperBound.getTime() + diff);
		} else {
			lowerBound2 = new Date(lowerBound.getTime() - diff);
			upperBound2 = new Date(upperBound.getTime() - diff);
		}

		Collection<?> listeners = getFieldLowerBound().getListeners(ValueChangeEvent.class);
		for (Object listener : listeners) {
			getFieldLowerBound().removeListener((ValueChangeListener) listener);
		}

		getFieldLowerBound().setValue(lowerBound2);
		getFieldUpperBound().setValue(upperBound2);

		for (Object listener : listeners) {
			getFieldLowerBound().addListener((ValueChangeListener) listener);
		}
	}

	@Override
	public void buttonClick(ClickEvent event) {
		if (btnPrevious == event.getButton()) {
			previous();
			return;
		}

		if (btnNext == event.getButton()) {
			next();
			return;
		}
	}

}
