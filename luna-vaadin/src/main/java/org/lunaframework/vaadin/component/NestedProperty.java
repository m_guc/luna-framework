package org.lunaframework.vaadin.component;

import java.util.Map;

import org.lunaframework.core.util.UtilsForReflection;

import com.vaadin.data.util.AbstractProperty;

public class NestedProperty extends AbstractProperty {

	private final String propertyName;
	private final Object instance;
	private Class<?> type;

	public NestedProperty(Object instance, String propertyName, Class<?> propertyType) {
		this.propertyName = propertyName;
		this.instance = instance;
		this.type = propertyType;
	}

	@Override
	public Object getValue() {
		return UtilsForReflection.getValue(instance, propertyName);
	}

	@Override
	public void setValue(Object newValue) {
		if (instance instanceof Map) {
			((Map) instance).put(propertyName, newValue);
			return;
		}
		UtilsForReflection.setValue(instance, propertyName, newValue);
	}

	public String getPropertyName() {
		return propertyName;
	}

	@Override
	public Class<?> getType() {
		return type;
	}

}
