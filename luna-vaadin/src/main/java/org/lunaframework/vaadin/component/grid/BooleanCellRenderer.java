package org.lunaframework.vaadin.component.grid;

import org.lunaframework.vaadin.component.NestedProperty;

public class BooleanCellRenderer extends AbstractGridCellRenderer {

	private final String trueLabel;
	private final String falseLabel;

	public BooleanCellRenderer(String trueLabel, String falseLabel) {
		this.trueLabel = trueLabel;
		this.falseLabel = falseLabel;
	}

	@Override
	protected String getCellValue(Object bean, Object cellValue, NestedProperty property) {
		if (cellValue == null) {
			return "";
		}
		Boolean b = (Boolean) cellValue;
		return (b) ? trueLabel : falseLabel;
	}

}
