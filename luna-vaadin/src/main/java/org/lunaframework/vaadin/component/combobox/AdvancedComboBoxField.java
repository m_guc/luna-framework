package org.lunaframework.vaadin.component.combobox;

import java.util.Collection;

import org.json.JSONArray;
import org.lunaframework.vaadin.component.combobox.AdvancedComboBox.SelectionChangeListener;

import com.vaadin.ui.Component;
import com.vaadin.ui.CustomField;

public class AdvancedComboBoxField extends CustomField<Collection> {

	public void setDisplayField(String fieldName) {
		getComboBox().setDisplayField(fieldName);
	}

	public void setIdField(String fieldName) {
		getComboBox().setIdField(fieldName);
	}

	public void setItems(Collection items) {
		getComboBox().setItems(items);
	}

	public void addSelectionChangeListener(SelectionChangeListener listener) {
		getComboBox().addSelectionChangeListener(listener);
	}

	protected AdvancedComboBox getComboBox() {
		return (AdvancedComboBox) getContent();
	}

	public void setInputPrompt(String placeHolder) {
		getComboBox().setPlaceHolder(placeHolder);
	}

	@Override
	public void setValue(Collection newValue) {
		super.setValue(newValue);
		getComboBox().setSelectedItems(newValue);
	}

	@Override
	protected void setInternalValue(Collection newValue) {
		super.setInternalValue(newValue);
		getComboBox().setSelectedItems(newValue);
	}

	@Override
	protected Component initContent() {
		return new AdvancedComboBox() {
			@Override
			protected void selectItems(JSONArray jsonArray) {
				super.selectItems(jsonArray);
				AdvancedComboBoxField.super.setValue(getComboBox().getSelectedItems());
			}
		};
	}

	@Override
	public Class<? extends Collection> getType() {
		return Collection.class;
	}

}
