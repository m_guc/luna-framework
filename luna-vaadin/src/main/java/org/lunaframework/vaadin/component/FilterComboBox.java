package org.lunaframework.vaadin.component;

import java.util.Collection;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.ui.AbsoluteLayout;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

/**
 * Combobox text change listener desteklemediği text change de çarpıyı
 * kaldıramadık...
 * 
 * @author yawo
 * 
 * @param <T>
 */
public class FilterComboBox<T> extends AbsoluteLayout {

	private BeanComboBox<T> combobox;
	private Button btnClear;

	public FilterComboBox(Class<T> clazz, String displayField) {
		this(clazz, null, displayField);
	}

	public FilterComboBox(Class<T> clazz, Collection<T> beans, String displayField) {
		setWidth("150px");
		setHeight("23px");
		combobox = new BeanComboBox<T>(clazz, beans, displayField);
		initComponents();
	}

	public void setBeans(Collection<T> beans) {
		combobox.setBeans(beans);
	}

	public T getSelectedBean() {
		return combobox.getSelectedBean();
	}

	@Override
	public void setImmediate(boolean immediate) {
		combobox.setImmediate(immediate);
	}

	public void setInputPrompt(String inputPrompt) {
		combobox.setInputPrompt(inputPrompt);
	}

	public void addValueChangeListener(ValueChangeListener listener) {
		combobox.addValueChangeListener(listener);
	}

	public void removeItem(T item) {
		combobox.removeItem(item);
	}

	public void removeAllItems() {
		combobox.removeAllItems();
	}

	public void setNullSelectionAllowed(boolean nullSelectionAllowed) {
		combobox.setNullSelectionAllowed(nullSelectionAllowed);
	}

	public void setSelectedBean(T item) {
		combobox.setSelectedBean(item);
	}

	public void focus() {
		combobox.focus();
	}

	private void initComponents() {

		combobox.setWidth("100%");
		combobox.addListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				Object value = event.getProperty().getValue();
				btnClear.setVisible(value != null);
			}
		});

		btnClear = new Button();
		btnClear.setVisible(false);
		btnClear.setStyleName("filter-combo");
		btnClear.addListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				combobox.setSelectedBean(null);
			}
		});
		btnClear.setWidth("25px");
		btnClear.setHeight("46px");
		addComponent(combobox, "left:0");
		addComponent(btnClear, "right:0");
	}
}
