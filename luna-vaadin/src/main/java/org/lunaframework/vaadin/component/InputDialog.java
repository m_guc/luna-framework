package org.lunaframework.vaadin.component;

import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.TextField;

public class InputDialog implements ClickListener {

	private TextField textField;
	private Dialog dialog;
	private boolean inputRequired;

	public InputDialog(String caption) {
		this("Bilgi", caption, false,"340px");
	}

	public InputDialog(String title, String caption, boolean inputRequired,String width) {
		this.inputRequired = inputRequired;

		textField = new TextField();
		textField.setWidth("100%");
		textField.setCaption(caption);
		textField.setRequired(inputRequired);

		Button btnOk = new Button("Tamam", this);

		dialog = new Dialog(title, textField);
		dialog.setWidth(width);
		dialog.setHeight("200px");
		dialog.addButton(btnOk);
		dialog.setVisible(true);
	}

	public String getText() {
		return (String) textField.getValue();
	}

	@Override
	public void buttonClick(ClickEvent event) {
		String text = getText();
		if (inputRequired && (text == null || text.isEmpty())) {
			return;
		}
		dialog.setVisible(false);
		onOk();
	}

	protected void onOk() {
		
	}
}
