package org.lunaframework.vaadin.component.grid;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import org.lunaframework.vaadin.component.NestedProperty;
import org.lunaframework.vaadin.component.PropertyDescriptor;

import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.Property.ValueChangeNotifier;
import com.vaadin.data.util.AbstractProperty;

public class SimpleBeanItem<T> implements Item, ValueChangeNotifier {

	private Map<String, Property> propertyMap;
	private Object itemId;
	private T bean;

	public SimpleBeanItem(Object itemId, T bean, Map<String, PropertyDescriptor> propertyDescriptorMap) {
		this.itemId = itemId;
		this.bean = bean;
		this.propertyMap = createPropertyMap(bean, propertyDescriptorMap);
	}

	private Map<String, Property> createPropertyMap(T bean,
			Map<String, PropertyDescriptor> propertyDescriptorMap) {
		Map<String, Property> propertyMap = new LinkedHashMap<String, Property>();

		Set<String> keySet = propertyDescriptorMap.keySet();
		for (String propertyName : keySet) {
			PropertyDescriptor<T> propertyDescriptor = propertyDescriptorMap.get(propertyName);
			propertyMap.put(propertyName, new NestedProperty(bean, propertyName, propertyDescriptor.getPropertyType()));
		}
		return propertyMap;
	}

	@Override
	public Property getItemProperty(Object id) {
		return getPropertyMap().get(id);
	}

	@Override
	public Collection<?> getItemPropertyIds() {
		return getPropertyMap().keySet();
	}

	@Override
	public boolean addItemProperty(Object id, Property property) throws UnsupportedOperationException {
		getPropertyMap().put((String) id, property);
		return true;
	}

	@Override
	public boolean removeItemProperty(Object id) throws UnsupportedOperationException {
		getPropertyMap().remove(id);
		return true;
	}

	@Override
	public void addListener(ValueChangeListener listener) {
		addValueChangeListener(listener);
	}

	@Override
	public void removeListener(ValueChangeListener listener) {
		removeValueChangeListener(listener);
	}

	public T getBean() {
		return bean;
	}

	public void setBean(T bean) {
		this.bean = bean;
	}

	public Object getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public Map<String, Property> getPropertyMap() {
		return propertyMap;
	}

	@Override
	public void addValueChangeListener(ValueChangeListener listener) {
		for (Property p : getPropertyMap().values()) {
			((AbstractProperty) p).addValueChangeListener(listener);
		}
	}

	@Override
	public void removeValueChangeListener(ValueChangeListener listener) {
		for (Property p : getPropertyMap().values()) {
			((AbstractProperty) p).removeValueChangeListener(listener);
		}
	}
}
