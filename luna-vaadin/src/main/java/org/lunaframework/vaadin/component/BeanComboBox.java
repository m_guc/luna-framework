package org.lunaframework.vaadin.component;

import java.util.Collection;
import java.util.Map;

import org.lunaframework.vaadin.component.grid.StringContainsFilter;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.filter.SimpleStringFilter;
import com.vaadin.event.FieldEvents.TextChangeEvent;
import com.vaadin.event.FieldEvents.TextChangeListener;
import com.vaadin.shared.ui.combobox.FilteringMode;
import com.vaadin.ui.ComboBox;

public class BeanComboBox<T> extends ComboBox {

	private boolean disableLocalFiltering;

	public BeanComboBox(Class<T> clazz, String displayField) {
		this(clazz, null, displayField);
	}

	public BeanComboBox(Class<T> clazz, Collection<T> beans, String displayField) {
		super();
		setContainerDataSource(new BeanItemContainer<T>(clazz, beans));
		if (displayField != null) {
			setItemCaptionPropertyId(displayField);
		}
		setFilteringMode(FilteringMode.CONTAINS);
	}

	public void setDisableLocalFiltering(boolean disable) {
		this.disableLocalFiltering = disable;
	}

	@Override
	protected Filter buildFilter(String filterString, FilteringMode filteringMode) {
		Filter filter = null;
		if (disableLocalFiltering) {
			return null;
		}

		if (null != filterString && !"".equals(filterString)) {
			switch (filteringMode) {
			case OFF:
				break;
			case STARTSWITH:
				filter = new SimpleStringFilter(getItemCaptionPropertyId(), filterString, true, true);
				break;
			case CONTAINS:
				filter = new StringContainsFilter((String) getItemCaptionPropertyId(), filterString);
				break;
			}
		}
		return filter;
	}

	public void addBean(T bean) {
		getContainer().addBean(bean);
	}

	public void setBeans(Collection<T> beans) {
		getContainer().addAll(beans);
	}

	public T getSelectedBean() {
		return (T) getValue();
	}

	public void setSelectedBean(T item) {
		if (item == null) {
			select(null);
		}
		select(item);
	}

	private BeanItemContainer<T> getContainer() {
		return (BeanItemContainer<T>) getContainerDataSource();
	}

	@Override
	public void changeVariables(Object source, Map<String, Object> variables) {
		if (variables.containsKey("filter")) {
			final String text = variables.get("filter").toString();
			fireEvent(new TextChangeEvent(this) {
				@Override
				public String getText() {
					return text;
				}

				@Override
				public int getCursorPosition() {
					return text.length();
				}
			});
		}
		super.changeVariables(source, variables);
	}

	public void addTextChangeListener(TextChangeListener listener) {
		addListener(TextChangeListener.EVENT_ID, TextChangeEvent.class, listener, TextChangeListener.EVENT_METHOD);
	}

	public void removeListener(TextChangeListener listener) {
		removeListener(TextChangeListener.EVENT_ID, TextChangeEvent.class, listener);
	}
}
