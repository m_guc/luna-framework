package org.lunaframework.vaadin.component.grid;

import java.util.Locale;

import org.lunaframework.vaadin.component.NestedProperty;

import com.vaadin.data.Container.Filter;
import com.vaadin.data.Item;
import com.vaadin.data.Property;

public class StringContainsFilter implements Filter {

	private static Locale localeTurkish = new Locale("tr", "TR");

	private final String propertyId;
	private final String filterString;

	public StringContainsFilter(String propertyId, String filterString) {
		this.propertyId = propertyId;
		this.filterString = filterString;
	}

	@Override
	public boolean passesFilter(Object itemId, Item item) throws UnsupportedOperationException {
		final Property p = (Property) item.getItemProperty(propertyId);

		Object propertyValue = p.getValue();

		if (p == null || propertyValue == null) {
			return false;
		}

		// String propertyStringValue = propertyValue.toString();
		String propertyStringValue = propertyValue == null ? "" : propertyValue.toString();

		if (p instanceof NestedProperty) {
			SimpleBeanItem beanItem = (SimpleBeanItem) item;
			propertyStringValue = getPropertyStringValue(beanItem.getBean(), propertyValue, (NestedProperty) p);
		}

		if (!propertyStringValue.toLowerCase(localeTurkish).contains(filterString.toLowerCase(localeTurkish))) {
			return false;
		}
		return true;
	}

	protected String getPropertyStringValue(Object bean, Object propertyValue, NestedProperty nestedProperty) {
		return propertyValue.toString();
	}

	public boolean appliesToProperty(Object propertyId) {
		return this.propertyId.equals(propertyId);
	}

}
