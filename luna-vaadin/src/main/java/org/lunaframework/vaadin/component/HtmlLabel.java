package org.lunaframework.vaadin.component;

import com.vaadin.ui.Label;

public class HtmlLabel extends Label {

	public HtmlLabel(String text) {
		setContentMode(Label.CONTENT_XHTML);
		setValue(text);
	}
}
