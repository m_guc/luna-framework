package org.lunaframework.vaadin.component.grid;

import java.util.Date;

import org.lunaframework.core.util.UtilsForDate;
import org.lunaframework.vaadin.component.NestedProperty;

public class DateCellRenderer extends AbstractGridCellRenderer {

	private final String dateFormat;
	

	public DateCellRenderer() {
		this("dd/MM/yyyy");
	}

	public DateCellRenderer(String dateFormat) {
		this.dateFormat = dateFormat;
	}

	@Override
	protected String getCellValue(Object bean, Object cellValue, NestedProperty property) {
		if (cellValue == null) {
			return "";
		}

		if (Date.class.isAssignableFrom(property.getType())) {
			return UtilsForDate.format((Date) cellValue, dateFormat);
		}
		return super.getCellValue(bean,cellValue, property);
	}
	
	@Override
	public String getCellValueAsString(Object bean, Object cellValue, NestedProperty property) {
		return getCellValue(bean, cellValue, property);
	}
}
