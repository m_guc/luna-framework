package org.lunaframework.vaadin.component.grid;

import java.io.Serializable;

import org.lunaframework.vaadin.component.NestedProperty;


public abstract class AbstractGridCellRenderer implements Serializable {

	protected String getCellValue(Object bean, Object cellValue, NestedProperty property) {
		if (cellValue == null) {
			return "";
		}
		return cellValue.toString();
	}

	public String getCellValueAsString(Object bean, Object cellValue, NestedProperty property) {
		if (cellValue == null) {
			return "";
		}
		return cellValue.toString();
	}
}
