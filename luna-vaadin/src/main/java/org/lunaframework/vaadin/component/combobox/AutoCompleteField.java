package org.lunaframework.vaadin.component.combobox;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.lunaframework.core.util.UtilsForReflection;

import com.vaadin.annotations.JavaScript;
import com.vaadin.annotations.StyleSheet;
import com.vaadin.ui.AbstractJavaScriptComponent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.JavaScriptFunction;

@JavaScript({ "AutoCompleteField.js", "jquery.min.js", "select2.min.js", "select2_locale_tr.js" })
@StyleSheet({ "select2.css", "select2-spinner.gif", "select2.png" })
public class AutoCompleteField<T> extends AbstractJavaScriptComponent {

	private final String idField;
	private final String displayField;
	private Map<String, T> idToItemMap = new HashMap<String, T>();

	private ItemQueryListener<T> itemQueryListener;

	private Collection<T> selectedItems = new ArrayList<>();

	public AutoCompleteField() {
		this("id", "text");
	}

	public AutoCompleteField(String idField, String displayField) {
		this.idField = idField;
		this.displayField = displayField;
		setWidth("100%");
		addCallbackFunctions();
	}

	private void addCallbackFunctions() {
		addFunction("selectItems", new JavaScriptFunction() {
			public void call(JSONArray arguments) throws JSONException {
				selectItems(arguments.getJSONArray(0));
			}
		});

		addFunction("queryItems", new JavaScriptFunction() {
			public void call(JSONArray arguments) throws JSONException {
				queryItems(arguments.getString(0));
			}
		});
	}

	public void queryItems(String queryString) {
		if (itemQueryListener == null) {
			return;
		}
		itemQueryListener.onQueryItems(queryString);
	}

	public void setItems(Collection<T> items) {
		idToItemMap = new HashMap<String, T>();
		getState().items = createRowItems(items);
		markAsDirty();
	}

	private List<Map<String, Object>> createRowItems(Collection<T> queryItems) {
		List<Map<String, Object>> rowItems = new ArrayList<Map<String, Object>>(queryItems.size());
		for (T item : queryItems) {
			idToItemMap.put(getIdValue(item), item);
			rowItems.add(createItemMap(item));
		}
		return rowItems;
	}

	private Map<String, Object> createItemMap(T item) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("id", getIdValue(item));
		map.put("text", getDisplayValue(item));
		return map;
	}

	public Collection<T> getSelectedItems() {
		return this.selectedItems;
	}

	protected void selectItems(JSONArray jsonArray) {
		this.selectedItems = new ArrayList<T>();
		getState().selectedItems = new ArrayList<Map<String, Object>>();

		for (int c = 0; c < jsonArray.length(); c++) {
			try {
				String object = (String) jsonArray.get(c);
				if (object != null) {
					object = object.trim();
				}
				T item = idToItemMap.get(object);
				this.selectedItems.add(item);
				getState().selectedItems.add(createItemMap(item));
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}

	private String getIdValue(T item) {
		return getValueAsString(item, idField);
	}

	protected String getDisplayValue(T item) {
		return getValueAsString(item, displayField);
	}

	protected String getValueAsString(T item, String fieldName) {
		if (item == null) {
			return null;
		}

		if (fieldName == null) {
			return item.toString();
		}

		Object value = UtilsForReflection.getValue(item, fieldName);
		if (value == null) {
			return null;
		}

		return value.toString().trim();
	}

	@Override
	protected AutoCompleteFieldState getState(boolean markAsDirty) {
		return (AutoCompleteFieldState) super.getState(markAsDirty);
	}

	@Override
	protected AutoCompleteFieldState getState() {
		return (AutoCompleteFieldState) super.getState();
	}

	public void setPlaceHolder(String placeHolder) {
		getState().placeHolder = placeHolder;
		markAsDirty();
	}

	/**
	 * this is a dummy fix for dropdown list cutoff in verticallayout
	 * 
	 * @return
	 */
	public HorizontalLayout getAsHorizontalLayout() {
		HorizontalLayout horizontalLayout = new HorizontalLayout(this);
		horizontalLayout.setWidth(this.getWidth(), this.getWidthUnits());
		return horizontalLayout;
	}

	public void setItemQueryListener(ItemQueryListener<T> itemQueryListener) {
		this.itemQueryListener = itemQueryListener;
	}

	public interface ItemQueryListener<T> {

		public void onQueryItems(String queryString);
	}
}
