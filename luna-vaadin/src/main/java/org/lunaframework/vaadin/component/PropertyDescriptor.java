package org.lunaframework.vaadin.component;

import com.vaadin.data.Property;
import com.vaadin.data.util.VaadinPropertyDescriptor;

public class PropertyDescriptor<T> implements VaadinPropertyDescriptor<T> {

	private final String name;
	private Class<?> propertyType;

	public PropertyDescriptor(Class propertyType, String name) {
		this.propertyType = propertyType;
		this.name = name;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public Class<?> getPropertyType() {
		return propertyType;
	}

	@Override
	public Property createProperty(T bean) {
		return new NestedProperty(bean, name, getPropertyType());
	}

}
