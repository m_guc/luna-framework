package org.lunaframework.vaadin.component;

import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;

public class LabeledPanel extends HorizontalLayout {

	public LabeledPanel(String label, Component component) {
		HtmlLabel htmlLabel = new HtmlLabel("<div style='padding:2px 3px 0 2px'>" + label + " : </div>");
		addComponent(htmlLabel);
		addComponent(component);
		setExpandRatio(component, 1.0f);
		setExpandRatio(htmlLabel, 0.2f);
	}
}
