package org.lunaframework.vaadin.component;

import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;

import org.apache.commons.lang.ArrayUtils;
import org.lunaframework.core.util.UtilsForReflection;
import org.lunaframework.vaadin.component.grid.PropertyDescriptorUtility;
import org.lunaframework.vaadin.component.grid.SimpleBeanItem;

import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.validator.NullValidator;
import com.vaadin.server.UserError;
import com.vaadin.ui.AbstractComponent;
import com.vaadin.ui.Component;
import com.vaadin.ui.Field;
import com.vaadin.ui.Form;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.VerticalLayout;

public class FormPanel<T> extends VerticalLayout {

	private Form form;
	private String[] propertyNames;
	private String[] propertyTitles;
	private String[] requiredFields;
	private Class<?> clazz;
	private Class[] propertyTypes;

	public FormPanel() {
		this(null, null, null);
	}

	public FormPanel(Class<?> clazz, String[] propertyNames, String[] propertyTitles) {
		this.clazz = clazz;
		this.propertyNames = propertyNames;
		this.propertyTitles = propertyTitles;

		addComponent(getForm());
	}

	@SuppressWarnings("deprecation")
	private Form getForm() {
		if (form == null) {
			form = new Form();
			// form.setFormFieldFactory(new DefaultFormFieldFactory());
			form.setFormFieldFactory(new AdvancedFormFieldFactory() {
				@Override
				public Field createField(Item item, Object propertyId, Component uiContext) {
					String propertyName = propertyId.toString();

					Field field = createFormField(propertyName, super.createField(item, propertyId, uiContext));
					int indexOf = ArrayUtils.indexOf(propertyNames, propertyId.toString());
					field.setCaption(propertyTitles[indexOf]);

					return field;
				}
			});
			form.setBuffered(false);

		}
		return form;
	}

	public void setBuffered(boolean buffered) {
		getForm().setBuffered(buffered);
	}

	public Object getFieldValue(String propertyName) {
		return getForm().getField(propertyName).getValue();
	}

	public void setFieldValue(String propertyName, Object newValue) {
		getForm().getField(propertyName).setValue(newValue);
	}

	protected Field createFormField(String propertyName, Field field) {
		return field;
	}

	public void commit() {
		getForm().commit();
	}

	public void discard() {
		getForm().discard();
	}

	public boolean isValid() {
		for (String propertyName : getPropertyNames()) {
			Field field = getForm().getField(propertyName);
			if (field instanceof AbstractComponent) {
				if (!field.isValid()) {
					((AbstractComponent) field).setComponentError(new UserError("Bu alan boş olamaz"));
				} else {
					((AbstractComponent) field).setComponentError(null);
				}
			}
		}
		return getForm().isValid();
	}

	public Set<Field> checkValid() {
		LinkedHashSet<Field> fieldSet = new LinkedHashSet<Field>();
		for (String propertyName : propertyNames) {
			Field field = getForm().getField(propertyName);
			if (field instanceof AbstractComponent) {
				if (!field.isValid()) {
					fieldSet.add(field);
					((AbstractComponent) field).setComponentError(new UserError("Bu alan boş olamaz"));
				} else {
					((AbstractComponent) field).setComponentError(null);
				}
			}
		}
		getForm().isValid();
		return fieldSet;
	}

	public String buildValidationErrorMessage(Collection<Field> invalidFields) {

		StringBuilder builder = new StringBuilder();
		builder.append("\n");
		for (Field field : invalidFields) {
			builder.append("* ").append(field.getCaption()).append("\n");
		}
		builder.append("\n");
		return builder.toString();

	}

	public void clearErrors() {
		for (String propertyName : getPropertyNames()) {
			Field field = getForm().getField(propertyName);
			if (field instanceof AbstractComponent) {
				((AbstractComponent) field).setComponentError(null);
			}
		}
	}

	public boolean showValidatonErrorNotification() {
		Set<Field> invalidFields = checkValid();
		if (invalidFields.size() == 0) {
			return true;
		}

		invalidFields.iterator().next().focus();

		StringBuilder builder = new StringBuilder();
		builder.append("Doldurulması gereken alanlar var \n").append(buildValidationErrorMessage(invalidFields));

		Notification.show(builder.toString(), Type.WARNING_MESSAGE);
		return false;
	}

	public void setRequiredFields(String... fieldNames) {
		this.requiredFields = fieldNames;
	}

	public void setFieldSettings() {
		if (this.requiredFields == null) {
			return;
		}

		for (String name : this.requiredFields) {
			getForm().getField(name).setRequired(true);
			getForm().getField(name).addValidator(new NullValidator("Bu alan boş olamaz", false));
		}
	}

	public T getBean() {
		return (T) UtilsForReflection.getValue((Item) getForm().getItemDataSource(), "bean");
	}

	public void setLayout(Layout layout) {
		getForm().setLayout(layout);
	}

	public void setBean(T bean) {
		getForm().setItemDataSource(createBeanItem(bean));
		setFieldSettings();
	}

	protected Item createBeanItem(T bean) {
		Item beanItem = null;
		if (getClazz() != null) {
			beanItem = new BeanItem<T>(bean, Arrays.asList(getPropertyNames()));
			// beanItem = new SimpleBeanItem<T>(bean, bean,
			// PropertyDescriptorUtility.createPropertyDescriptorsByClazz(getClazz(),
			// getPropertyNames()));
		} else if (getPropertyTypes() != null) {
			beanItem = new SimpleBeanItem<T>(bean, bean, PropertyDescriptorUtility.createPropertyDescriptors(getPropertyTypes(), getPropertyNames()));
		}
		return beanItem;
	}

	public void setFieldEnabled(String propertyName, boolean enabled) {
		getForm().getField(propertyName).setEnabled(enabled);
	}

	public void setComboBoxFieldItems(String propertyName, Collection<?> items) {
		Field field = getForm().getField(propertyName);
		if (field instanceof BeanComboBox) {
			((BeanComboBox) field).setBeans(items);
		}
	}

	public Layout getLayout() {
		return getForm().getLayout();
	}

	public Field getField(String propertyName) {
		return getForm().getField(propertyName);
	}

	public void setEditable(boolean editable) {
		for (String propertyName : getPropertyNames()) {
			Field field = getField(propertyName);
			if (field == null) {
				continue;
			}

			field.setReadOnly(!editable);
		}
	}

	public Class<?> getClazz() {
		return clazz;
	}

	public void setClazz(Class<?> clazz) {
		this.clazz = clazz;
	}

	public Class[] getPropertyTypes() {
		return propertyTypes;
	}

	public void setPropertyTypes(Class[] propertyTypes) {
		this.propertyTypes = propertyTypes;
	}

	public String[] getPropertyNames() {
		return propertyNames;
	}

	public void setPropertyNames(String[] propertyNames) {
		this.propertyNames = propertyNames;
	}

	public String[] getPropertyTitles() {
		return propertyTitles;
	}

	public void setPropertyTitles(String[] propertyTitles) {
		this.propertyTitles = propertyTitles;
	}

}
