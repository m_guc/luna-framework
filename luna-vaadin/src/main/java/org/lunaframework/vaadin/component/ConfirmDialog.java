package org.lunaframework.vaadin.component;

import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Label;

public class ConfirmDialog implements ClickListener {

	private final String title;
	private final String text;

	private Dialog dialog;
	private String okLabel;
	private String cancelLabel;
	private Button btnOk;
	private Button btnCancel;

	private ConfirmationListener confirmationListener;

	public ConfirmDialog(String title, String text) {
		this(title, text, "Tamam", "İptal");
	}

	public ConfirmDialog(String title, String text, String okLabel, String cancelLabel) {
		this.title = title;
		this.text = text;
		this.okLabel = okLabel;
		this.cancelLabel = cancelLabel;
		initComponents();
	}

	public void setConfirmationListener(ConfirmationListener confirmationListener) {
		this.confirmationListener = confirmationListener;
	}

	private void initComponents() {
		dialog = new Dialog(title, new Label(text, ContentMode.HTML));
		dialog.setWidth("300px");
		dialog.setHeight("150px");
		dialog.setResizable(false);
		dialog.setClosable(false);

		btnOk = new Button(okLabel, this);
		btnCancel = new Button(cancelLabel, this);

		dialog.addButton(btnOk);
		dialog.addButton(btnCancel);
	}

	public void setWidth(String width) {
		dialog.setWidth(width);
	}

	public void setHeight(String height) {
		dialog.setHeight(height);
	}

	protected boolean onOk() {
		return true;
	}

	public void show() {
		dialog.setVisible(true);
	}

	public void hide() {
		dialog.setVisible(false);
	}

	public void buttonClick(ClickEvent event) {
		if (btnOk == event.getButton()) {
			dialog.setVisible(!onOk());
			if (confirmationListener != null)
				confirmationListener.onConfirm();
			return;
		}
		if (btnCancel == event.getButton()) {
			dialog.setVisible(false);
			if (confirmationListener != null)
				confirmationListener.onCancel();
			return;
		}
	}

	public static interface ConfirmationListener {
		public void onConfirm();

		public void onCancel();
	}

}
