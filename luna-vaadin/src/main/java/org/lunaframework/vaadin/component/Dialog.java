package org.lunaframework.vaadin.component;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

public class Dialog {

	private ButtonPanel buttonPanel;

	private VerticalLayout panel;

	private Window window = new Window();

	public Dialog(String title, final Component component) {
		window.setCaption(title);
		window.setModal(true);

		HorizontalLayout horizontalLayout = new HorizontalLayout();
		horizontalLayout.setSizeFull();
		horizontalLayout.addComponent(component);

		getPanel().addComponent(horizontalLayout);
		getPanel().setExpandRatio(horizontalLayout, 3.0f);

		window.setContent(getPanel());
		UI.getCurrent().addWindow(window);
	}

	private VerticalLayout getPanel() {
		if (panel == null) {
			panel = new VerticalLayout();
			panel.setSizeFull();
			panel.setSpacing(true);
			panel.setMargin(true);
		}
		return panel;
	}

	public void setWidth(String width) {
		window.setWidth(width);
	}

	public void setHeight(String height) {
		window.setHeight(height);
	}

	public void setModal(boolean modality) {
		window.setModal(modality);
	}
	
	public void setClosable(boolean closable) {
		window.setClosable(closable);
	}
	
	private ButtonPanel getButtonPanel() {
		if (buttonPanel == null) {
			buttonPanel = new ButtonPanel();
		}
		return buttonPanel;
	}

	public void addButton(Button... buttons) {
		if (buttonPanel == null) {
			getPanel().addComponent(getButtonPanel());
			getPanel().setExpandRatio(getButtonPanel(), 0.0f);
			getPanel().setComponentAlignment(getButtonPanel(), Alignment.BOTTOM_RIGHT);
		}
		getButtonPanel().addButtons(buttons);
	}

	public void setVisible(boolean visible) {
		window.setVisible(visible);
	}

	public void destroy() {
		UI.getCurrent().removeWindow(window);
	}

	public void setResizable(boolean resizable) {
		window.setResizable(resizable);
	}

}
