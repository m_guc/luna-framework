package org.lunaframework.vaadin;

import java.net.SocketException;

import org.apache.commons.lang.exception.ExceptionUtils;

import com.vaadin.server.AbstractErrorMessage;
import com.vaadin.server.DefaultErrorHandler;
import com.vaadin.server.ErrorMessage;
import com.vaadin.ui.AbstractComponent;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;

public class DefaultSystemErrorHandler extends DefaultErrorHandler {
	@Override
	public void error(com.vaadin.server.ErrorEvent event) {
		Throwable throwable = event.getThrowable();
		Throwable rootCause = ExceptionUtils.getRootCause(throwable);
		rootCause = (rootCause == null ? throwable : rootCause);

		if (throwable instanceof SocketException) {
			return;
		}
		AbstractComponent component = findAbstractComponent(event);
		if (component != null) {
			// Shows the error in AbstractComponent
			ErrorMessage errorMessage = AbstractErrorMessage.getErrorMessageForException(throwable);
//			component.setComponentError(errorMessage);
		}

		if (rootCause != null) {
			String message = rootCause.getMessage();
			if (rootCause instanceof NullPointerException) {
				message = "Genel bir sistem hatası oluştu";
				throwable.printStackTrace();
			}
			Notification.show(message, Type.ERROR_MESSAGE);
		}
	}
}