package org.lunaframework.vaadin.listener;

import com.vaadin.event.FieldEvents.BlurEvent;
import com.vaadin.event.FieldEvents.BlurListener;
import com.vaadin.event.FieldEvents.FocusEvent;
import com.vaadin.event.FieldEvents.FocusListener;
import com.vaadin.event.ShortcutListener;
import com.vaadin.ui.AbstractTextField;

/**
 * TODO burada text field ı parametre olarak vermek çok iyi olmadı kendi text
 * Field sınıfımızı yazmamız lazım
 * 
 * @author yawo
 * 
 */
public abstract class KeyListener extends ShortcutListener {

	private AbstractTextField field;
	private boolean hasFieldFocus = false;

	public KeyListener(AbstractTextField field, int keyCode) {
		super("", keyCode, new int[0]);
		this.field = field;
		this.field.addListener(new FocusListener() {
			@Override
			public void focus(FocusEvent event) {
				hasFieldFocus = true;
			}
		});
		this.field.addListener(new BlurListener() {
			@Override
			public void blur(BlurEvent event) {
				hasFieldFocus = false;
			}
		});

		this.field.addShortcutListener(this);
	}

	@Override
	public void handleAction(Object sender, Object target) {
		if (hasFieldFocus) {
			this.handleKeyAction(sender, target);
		}
	}

	protected abstract void handleKeyAction(Object sender, Object target);

}
