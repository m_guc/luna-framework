package org.lunaframework.vaadin.event;

import org.lunaframework.vaadin.mediator.MediatorEvent;
import org.lunaframework.vaadin.mediator.MediatorEventHandler;

import com.vaadin.ui.Component;

public class SwitchContentEvent extends MediatorEvent {

	private final Class<? extends Component> componentClass;

	public SwitchContentEvent(Class<? extends Component> componentClass) {
		this.componentClass = componentClass;
	}

	@Override
	public void dispatch(MediatorEventHandler handler) {
		if (handler instanceof SwitchContentHandler) {
			((SwitchContentHandler) handler).onSwitchContent(this);
		}
	}

	public Class<? extends Component> getComponentClass() {
		return componentClass;
	}

	public interface SwitchContentHandler extends MediatorEventHandler {

		public void onSwitchContent(SwitchContentEvent event);
	}

}
