package org.lunaframework.vaadin;

import java.text.NumberFormat;
import java.util.Locale;

import com.vaadin.data.util.converter.StringToLongConverter;

/*
 * Sadece strşng ten long'a dönüştürür, düzeltilmeli
 */
public class DefaultStringToNumberConverter extends StringToLongConverter{

	@Override
	protected NumberFormat getFormat(Locale locale) {
		NumberFormat format = super.getFormat(locale);
		format.setGroupingUsed(false);
		return format;
	}

	@Override
	public String convertToPresentation(Long value, Class<? extends String> targetType, Locale locale)
			throws com.vaadin.data.util.converter.Converter.ConversionException {
		if (value == null) {
			return "";
		}
		return String.valueOf(value);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long convertToModel(String value, Class<? extends Long> targetType, Locale locale)
			throws com.vaadin.data.util.converter.Converter.ConversionException {
		if (!getModelType().isAssignableFrom(targetType)) {
			throw new ConversionException("Converter only supports " + getModelType().getName() + " (targetType was " + targetType.getName() + ")");
		}

		if (value == null || value.trim().isEmpty()) {
			return null;
		}
		return org.springframework.util.NumberUtils.parseNumber(value, targetType);
	}
}