package org.lunaframework.vaadin;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Locale;
import java.util.Set;

import com.vaadin.data.util.converter.Converter;

public class CollectionToSetConverter implements Converter<Collection, Set> {

	@Override
	public Set convertToModel(Collection value, Class<? extends Set> targetType, Locale locale)
			throws com.vaadin.data.util.converter.Converter.ConversionException {
		if (value == null) {
			return null;
		}
		return new LinkedHashSet(value);
	}

	@Override
	public Collection convertToPresentation(Set value, Class<? extends Collection> targetType, Locale locale)
			throws com.vaadin.data.util.converter.Converter.ConversionException {
		if (value == null) {
			return null;
		}
		return new ArrayList(value);
	}

	@Override
	public Class<Set> getModelType() {
		return Set.class;
	}

	@Override
	public Class<Collection> getPresentationType() {
		return Collection.class;
	}

}