org_lunaframework_vaadin_component_combobox_AdvancedComboBox = function() {

	
	var element = this.getElement();
	var placeholder = this.getState().placeHolder;
	
	var combo = $(
			"<select multiple='multiple' data-placeholder='"
					+ (placeholder == null ? "Choose an option" : placeholder)
					+ "'></select>").appendTo(element);
	var self = this;
	self.items = [];
	
	combo.select2({width:"100%"}).change(function(e) {		
		var items = combo.select2('data');
		var selectedItems = [];
		for (var c in items) {
			var item = items[c];
			selectedItems.push(item.id);
		}
		self.selectItems(selectedItems);
	});
	
	
	this.setItems = function(items) {
		self.items =items;
		combo.select2("val", []);
		combo.children("option").remove();
		for ( var c in items) {
			var item = items[c];
			$("<option value='" + $.trim(item.id) + "'>" + $.trim(item.text) + "</option>")
					.appendTo(combo);
		}
	};

	this.setSelectedItems = function(items) {
		var selectedItems = [];
		for (var c in items) {
			var id = items[c];
			selectedItems.push(getItemById(id));
		}
		combo.select2("data", selectedItems);
	};
	
	function getItemById(id){
		for (var k in self.items) {
			var item = self.items[k];
			if(item.id==id){
				return item;
			}
		}
	};
	
	this.onStateChange = function(){
		this.setItems(self.getState().items);
		this.setSelectedItems(self.getState().selectedItems);
		combo.attr("data-placeholder",self.getState().placeHolder);
	};

}