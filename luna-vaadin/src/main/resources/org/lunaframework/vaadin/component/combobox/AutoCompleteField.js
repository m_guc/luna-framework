org_lunaframework_vaadin_component_combobox_AutoCompleteField = function() {

	var element = this.getElement();
	var placeholder = this.getState().placeHolder;

	var combo = $(
			"<input type='hidden' data-placeholder='"
					+ (placeholder == null ? "Choose one" : placeholder)
					+ "' />").appendTo(element);
	var self = this;
	var obj = {};
	self.items = [];

	var callback;
	var options = {
		width : "100%",
		multiple : true,
		query : function(query) {
			if (query.term.length < 2) {
				return;
			}
			delay(function() {
				callback = query.callback;
				self.queryItems(query.term);
			}, 500);
		}
	}

	combo.select2(options).change(function(e) {
		var items = combo.select2('data');
		console.warn(items);
		var selectedItems = [];
		for ( var c in items) {
			var item = items[c];
			selectedItems.push(item.id);
		}
		self.selectItems(selectedItems);
	});

	this.setSelectedItems = function(items) {
		var selectedItems = [];

		for (var c = 0; c < items.length; c++) {
			var id = items[c];
			var item = getItemById(id);
			console.warn("ID : " + id + " ITEM : " + item);
			selectedItems.push(item);
		}

		if (selectedItems != null) {
			combo.select2("data", items);
		}
	};

	function getItemById(id) {
		for ( var k in self.items) {
			var item = self.items[k];
			if (item.id == id) {
				return item;
			}
		}
	}

	this.onStateChange = function() {
		console.log("state changed: "+self.getState().items);
		
		if (callback != null) {
			var opt = {
				results : [],
				more : false
			};
			opt.results = self.getState().items;
			
			if (opt.results != null) {
				callback(opt);
			}
		}
		combo.attr("data-placeholder", self.getState().placeHolder);
	};

	var delay = (function() {
		var timer = 0;
		return function(callback, ms) {
			clearTimeout(timer);
			timer = setTimeout(callback, ms);
		};
	})();

}